﻿
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" ng-app="FRAME">
<head>
    <title>FRAME</title>
    <link rel="shortcut icon" href="image/tool-logo.png" type="image/x-icon" />
    <link rel="manifest" href="manifest.json"></link>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="mobile-web-app-capable"         content="yes">
    <meta name="apple-mobile-web-app-capable"   content="yes">

    <script src="https://www.google.com/jsapi"           type="text/javascript"></script>

    <link href="./tools/css/font-awesome-4.2.2/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://use.fontawesome.com/f2c3eeb0bb.js"></script>
    
    <link href="./tools/css/gen/style.css" type="text/css" rel="stylesheet"></link>
    <link href="./css/gen/style.css" type="text/css" rel="stylesheet"></link>
    <link rel="stylesheet" href="./tools/bower_components/angular-material/angular-material.min.css">

    <!-- MIN START -->
    <script src="./tools/bower_components/angular/angular.min.js"                       type="text/javascript"></script> 
    <script src="./tools/bower_components/angular-route/angular-route.min.js"           type="text/javascript"></script> 
    <script src="./tools/bower_components/re-tree/re-tree.min.js"                       type="text/javascript"></script>
    <script src="./tools/bower_components/ng-device-detector/ng-device-detector.min.js" type="text/javascript"></script>
    <script src="./tools/bower_components/ngtouch/build/ngTouch.min.js"                 type="text/javascript"></script>
    <script src="./tools/bower_components/ng-file-upload/ng-file-upload-all.min.js"     type="text/javascript"></script> 

    <script src="./js/app.js"                                                           type="text/javascript"></script>
    <script src="./js/gen/tpl.min.js"                                                   type="text/javascript"></script>
    <script src="./tools/js/gen/tpl.min.js"                                             type="text/javascript"></script>
    <script src="./tools/js/service.js"                                                 type="text/javascript"></script>
    <script src="./tools/js/directive.js"                                               type="text/javascript"></script>
    <script src="./tools/js/filter.js"                                                  type="text/javascript"></script>
    <script src="./tools/js/lib/sockjs.min.js"                                          type="text/javascript"></script>
    <script src="./tools/js/lib/chart.js"                                               type="text/javascript"></script>
    <script src="./tools/js/lib/list.js"                                                type="text/javascript"></script>
    <script src="./tools/js/lib/fileTrans.js"                                           type="text/javascript"></script>
    <script src="./js/ctrl.js"                                                          type="text/javascript"></script>
    <!-- MIN END -->
<!--
    <script src="./js/gen/min.js" type="text/javascript"></script>
-->

<!--
    <script src="./js/mob.js"       type="text/javascript"></script>
-->
</head>

<body ng-controller="appCtrl">
    <div id="container">
        <div class="underLayer">
            <div class="over"></div>
        </div>
        <div header></div>
        <div menu style="position:relative; z-index:10;"></div>
        <div style="position:fixed; top:0; left:0;z-index:10;" menu></div>
        <div id="contentRouter" content-router></div>
        <div mobile></div>
        <div id="push"></div>
        <div notifier></div>
    </div>
    
    <div ng-if="!isMobile" id="footer">
        <a href="http://dev.jktools.com/">
            <div id="jktoolsImg"></div>
        </a>
    </div>

    <div id="facebook"></div>
    <div style="display:none; position:fixed; bottom :30px; z-index:10000; min-width:300px; min-height:30px; border:1px solid red; background:white;" id="status"> </div>
</body>

</html>
