var INFO = console.info;
var WARN = console.warn;
var ERR = console.error;

function pad2(input) {
    typeof input != 'undefined' || input || (input = 0);
    input <10 && (input = '0' + input);
    return input;
};

APP 
.controller('appCtrl', ['$scope', 'deviceDetector', function($scope, deviceDetector) {
    $scope.isMobile = deviceDetector.isMobile();
}])
.directive('land', ['FTPL', 'notifSvc', 'loginSvc', function(FTPL, notifSvc, loginSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.dctrl_land,
        link: function($scope, el, attrs) {
            $scope.popMe = function(edtme) {
                if (edtme == 'save') {
                    $scope.doEdit(false);
                }
            }

        }
    }
}])
.directive('product', ['FTPL', 'notifSvc', 'loginSvc', 'dragSvc', function(FTPL, notifSvc, loginSvc, dragSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.product_main,
        link: function($scope, el, attrs) {
            $scope.setProdIdx = function(idx, loc) { 
                $scope.prodIdx = idx; 
                $scope.locName = loc; 
            }

            $scope.hasImg = false;
            typeof $scope.nav == 'undefined' ? $scope.nav = { catIdx : '' } : $scope.nav.catIdx = '';

            $scope.prodIdx = 0;
            $scope.locName = { lab: 'Lola Main'}; //Hack for now
            $scope.nav.catIdx = 0;

            /*
            $scope.addCat = function(idx) {
                if (typeof idx == 'undefined') {
                    notifSvc.warn('Please select location', 3); 
                } else {
                    console.log( idx, ' prod idx', $scope.cms.locs[idx] );

                    if (typeof $scope.cms.locs[idx].cat == 'undefined')
                        $scope.cms.locs[idx].cat = [];

                    $scope.cms.locs[idx].cat.push({lab:'', items:[]});

                    console.log( $scope.cms.locs[idx].cat );
                }
            }
            */
        }
    }
}])
.directive('about', ['FTPL', 'notifSvc', 'loginSvc', 'dragSvc', 'api', function(FTPL, notifSvc, loginSvc, dragSvc, api) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.dctrl_about,
        link: function($scope, el, attrs) {
            console.log( 'ftpl about link');
            $scope.reorderZ = function() {
                console.log( 'REORDER in about');
                console.log( $scope.cms.staff, ' staff ');
            }

            $scope.saveOrder = function() {
                //$scope.cb({arg: $scope.saveOrderCb});
                $scope.doEdit();
            }

            var reorderTpl = '<div ng-if="reorder" reorder order-list="orderSubList" handle="head" save-order="saveOrder(arg)"></div>';
            $scope.pencilClick = function() {
                console.log( ' pencil click about');
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, FTPL.reorderTpl);
            }

            $scope.reorder = function(list) {
                $scope.orderSubList = $scope.cms.staff
                console.log('orderSubList ', $scope.orderSubList);
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, reorderTpl, 'topRight');
            }

            function loadBlogs() {
                api.get('blog/getBlogs', function(blogResp) {
                    var blogs = blogResp.data;
                    var bb = blogResp.data.map( function(b) {
                        //console.log( b );
                        return {
                            //url: '/#/blog/items?id=' + b.uid, 
                            url: '/blog/' + b.handle,
                            text: b.title.substring(0, 30) + (b.title.length > 30 ? ' ...' :'')
                       };
                    });
                    $scope.selOpts = bb;
                });
            };
            //loadBlogs();
            loginSvc.runAfterLogin( loadBlogs );
        }
    }
}])
.directive('contact', ['FTPL', 'notifSvc', 'loginSvc', function(FTPL, notifSvc, loginSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.contact_main,
        link: function($scope, el, attrs) {
            setTimeout( function() {
                console.log( $scope.cms );
                $scope.mapCoords = $scope.cms.locs[0];
                $scope.$$phase || $scope.$digest();
            }, 400);

            //$scope.mapCoords = $scope.cms.milCoords;
            //$scope.mapLab = $scope.cms.milLab;

            $scope.selectMap = function(arg) {
                console.log( arg );
                $scope.mapCoords = $scope.cms.locs[arg];
            }
        }
    }
}])
.directive('menu', ['TPL', function(TPL) {
    return {
        restrict: 'AC',
        replace: true,
        template: TPL.menu_main,
        link: function($scope, el, attrs) {
            console.log( el );
            typeof $scope.nav == 'undefined' && ($scope.nav = {});
            console.info( ' menu directive ' );
            $scope.openSub = function(subName) {
                console.log( subName , 'menuSub name ');
                $scope.nav.subMenu = $scope.nav.subMenu == subName ? '' :  subName;
            }
        }
    }
}])
.directive('footer', ['FTPL', 'notifSvc', function(FTPL, notifSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.footer,
        link: function($scope, el, attrs) {

            $scope.delLink = function(idxA) {
                console.log( 'footer delLink', idxA, 'abc');
                notifSvc.warn('Remove Link, Please confirm', 10, function(yes) {
                    if (yes) 
                        $scope.cms.links[idxA[0]].splice(idxA[1],1);
                    else
                        notifSvc.info('Remove Link aborted ', 3);
                });
            }
        }
    }
}])
.directive('header', ['$http', 'config', 'TPL', 'loginSvc', function($http, config, TPL, loginSvc) {
    return {
        restrict: 'AC',
        replace: true,
        template: TPL.tools_header,
        link: function($scope, el) {
        }
    }
}])
.directive('lax', [function() {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, el, attrs) {
            window.addEventListener('scroll', function(e) {
               el.css("transform","translateY(" + (1*window.pageYOffset)/2 + "px)");
            });
        }
    }
}])
.directive('reorder', ['FTPL', function(FTPL) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.reorder,
        scope: { orderList: "=", opts: "=", saveOrder:"&"},
        link: function($scope, el, attrs) {
            $scope.handle = typeof attrs.handle != 'undefined' && attrs.handle;

            $scope.save = function() { 
                $scope.saveOrder();
            }
        }
    }
}])
.directive('smoothSide', [function() { // requires jquery
    return {
        restrict: 'A',
        replace: false,
        scope: { scrollTo: "@" },
        compile: function(el, attrs) {
            if (typeof $ == 'undefined') { // make sure jquery is in
                var scr = document.createElement('script');
                scr.src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js";
                document.body.appendChild( scr );
            }

            return  function($scope, $elm, attrs) { //linker
                $elm.on('click', function() {
                    $('html,body').animate({scrollTop: $($scope.scrollTo).offset().top }, "slow");
                }); 
            }
        }
    }
}])
.directive('smoothScroll', [function() { // requires jquery
    return {
        restrict: 'A',
        replace: false,
        scope: { scrollTo: "@" },
        compile: function(el, attrs) {
            if (typeof $ == 'undefined') { // make sure jquery is in
                var scr = document.createElement('script');
                scr.src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js";
                document.body.appendChild( scr );
            }

            return  function($scope, $elm, attrs) { //linker
                $elm.on('click', function() {
                    $('html,body').animate({scrollTop: $($scope.scrollTo).offset().top }, "slow");
                }); 
            }
        }
    }
}])
.directive('stock_searchDepr', ['FTPL', 'api','loginSvc', '$location', function(FTPL, api, loginSvc, $location) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.stock_search,
        scope:false,
        link: function($scope, el) {
        }
    }
}])
.directive('settings', ['FTPL', 'LS', 'loginSvc','api', 'notifSvc', function(FTPL, LS, loginSvc, api, notifSvc ) {
    return {
        restrict: 'AC',
        replace: false,
        template: FTPL.settings,
        link: function($scope, el, attrs) {
            var routesUid;
            INFO(' settings ctrl', $scope.edt, $scope.auth.loggedIn);
            $scope.auth.loggedIn || $scope.popLogin();

            var myHost = document.location.hostname;
            function loadRoutes() {
                if ( false )
                api.get('gen/getRoutes', 'host=' + myHost, function(ret) {
                    INFO("ROUTES", ret, ret.data.routes); 
                    $scope.routes = ret.data.routes;
                    INFO(" scope routes " , $scope.routes );
                    routesUid     = ret.data.uid;
                });
                else
                api.get('./js/gen/routeConf.json', function(ret) {
                    console.log( ret );
                    $scope.routes = ret;
                    INFO( 'new routes ');
                });
            }
            loadRoutes();

            $scope.millHoursLab = ' milton hours ';
            $scope.hourAction = function(typ, item, key) {
                console.log( arguments , ' hour aciton');
            }

            function saveRoutes() {
                var data = {routes: $scope.routes, host: myHost};
                routesUid && (data.uid = routesUid);
                api.post('gen/saveRoutes', data, loadRoutes);
            }

            $scope.tabAction = function(typ, valO, key ) {
                INFO(typ, valO, ' save item aciton');

                if (typ == 'save') {
                    if (typeof valO.name != 'undefined' && valO.name != '') {
                        var found = $scope.routes.filter( function(r, i) { 
                            if (valO.name == r.name) {
                                $scope.routes[i] = valO;
                                return true;
                            } else  
                                return false;
                        });
                        found.length || $scope.routes.push(valO);
                        saveRoutes();
                    } else 
                        notifSvc.warn('Key/Name column cannot be empty', 5);
                }

                if (typ == 'del') {
                    INFO(' del here ');
                    $scope.routes.filter( function(r,i) {
                        r.name == valO.name && $scope.routes.splice(i,1);
                        return r.name == valO.name;
                    }).length > 0 && saveRoutes();
                }
            }
        }
    }
}])
.directive('cmsMap', ['FTPL', '$http', 'dragSvc', 'notifSvc', function(FTPL, $http, dragSvc, notifSvc) {
    return {
        restrict: 'AC',
        replace: true,
        scope: {coords: '=', lab: '=', edt: '=', cb: '&', allLocs:'='},
        template: FTPL.cms_map,
        compile: function(el, attrs) {
            var width, height, scale;
            var byImg = typeof attrs.mapByImg != 'undefined';
            var zoom = typeof attrs.zoom == 'undefined' ? 16 : parseInt(attrs.zoom);


            if (typeof attrs.scale == 'undefined') {
                width = '240';
                height = '240';
                scale = '1';
            } else {
                width = '640';
                height = '240';
                scale = '2';
                byImg = true; 
                zoom = typeof attrs.zoom == 'undefined' ? 15 : parseInt(attrs.zoom);
            }

            typeof window.GLOB == 'undefined' && ( window.GLOB = {cbs: []} );
            if (typeof window.GLOB.mapReady != 'function') 
                window.GLOB.mapReady = function(cb) {
                    if( arguments.length) 
                        typeof cb == 'function' && window.GLOB.cbs.push(cb);
                    else
                        window.GLOB.cbs.map( function(f) { f(); });
                }

            var scriptEl = document.getElementById( "googleMapAPI" );
            if (!scriptEl) {
                var scr = document.createElement('script');
                scr.id = "googleMapAPI";
                scr.src = "https://maps.googleapis.com/maps/api/js?callback=window.GLOB.mapReady&key=AIzaSyAf5LDtDQ755R8msq3DN1ubWQQxoc3M_Y0";
                document.body.appendChild( scr );
            }

            return function($scope, el, attrs) {
                $scope.pencilClick = function() {
                    $scope.edtme = !$scope.edtme;
                    dragSvc.init($scope, el, FTPL.cms_map_pop);
                }

                $scope.del = function() {
                    console.log( 'del map', $scope );
                    console.log( 'del map locs', $scope.allLocs, attrs.idx );
                    //$scope.coords = null;
                    $scope.allLocs.splice(parseInt(attrs.idx), 1);
                    //$scope.cb({arg: $scope.saveText});
                }

                $scope.saveText = function(sucs) {
                    if (sucs) {
                        notifSvc.ok('Saved');
                        $scope.pencilClick();
                    }
                    else {
                        notifSvc.warn('Something went wrong');
                    }
                }

                $scope.mapByImg = byImg;

                var tmHandle = null;
                $scope.myMap = function() {
                    resizeHandle = null;
                    if ( typeof $scope.coords == 'undefined' || typeof google == 'undefined') {
                        tmHandle = setTimeout( $scope.myMap, 100 );
                    } else {
                        clearTimeout( tmHandle );

                        var mapEl =  el.find('map')[0];
                        setTimeout( function() {
                            var coords = $scope.coords;
                            var gMap = google.maps;

                            typeof attrs.scale == 'undefined' || (width = parseInt(window.innerWidth/2));
                            var mapUrl = "https://maps.google.com/maps/api/staticmap?sensor=false&" + 
                                         "center=" + coords.lat + "," + coords.lon + "&zoom=" + zoom  +
                                         "&size=" + width +  "x" + height + 
                                         '&key=AIzaSyDRiLaPRVthLdpPdbr4WCvkIImBCaImf08' +
                                         "&markers=color:red|label:L|" + coords.lat + ',' + coords.lon + '&scale=' + scale;

                            if (typeof gMap != 'undefined') {
                                if ($scope.mapByImg) {
                                    $scope.mapUrl = mapUrl;
                                } else {
                                    var mapProp= {
                                        center:new google.maps.LatLng($scope.coords.lat, $scope.coords.lon),
                                        zoom:zoom,
                                    };

                                    var map=new google.maps.Map(mapEl, mapProp);
                                    var marker = new google.maps.Marker({
                                        position: {lat: $scope.coords.lat, lng: $scope.coords.lon},
                                        map: map,
                                        title: 'Welcome to Lola Rosa!'
                                    });
                                }
                                $scope.$$phase ||$scope.$digest();
                            }
                        }, 0);
                    }
                }

                 if (scriptEl && typeof google.maps != 'undefined')
                        $scope.myMap();
                else
                    window.GLOB.mapReady($scope.myMap);

                var resizeHandle = null;
                window.onresize = function() {
                    if (!resizeHandle && window.innerWidth < 1280)
                        resizeHandle = setTimeout( $scope.myMap, 500);
                }

                var geocoder;
                $scope.locAddress = function(addr, cb) {
                    typeof geocoder == 'undefined' && (geocoder = new google.maps.Geocoder());
                    var addr = $scope.coords.addr;
                    var geoUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address=' +  addr + '&key=AIzaSyAf5LDtDQ755R8msq3DN1ubWQQxoc3M_Y0';
                    if (typeof addr != 'undefined' && addr != '') 
                        $http.get(geoUrl).then(function(resp) {
                            console.log( resp.data.results[0].geometry.location, ' Json resp ' );
                            var loc = resp.data.results[0].geometry.location;
                            $scope.coords.lat = loc.lat
                            $scope.coords.lon = loc.lng;
                            $scope.myMap();
                        });
                     else 
                        $scope.myMap(); 
                }

                $scope.$watch('coords', function(n,o) {
                    typeof n == 'undefined' || $scope.myMap();
                });
            }
        }
    }
}])
.directive('dashboard', ['$http', 'TPL', function($http, TPL) {
    return {
        restrict: 'AC',
        replace: true,
        scope: false,
        template: TPL.tools_dashboard,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('options', ['TPL', function(TPL) { return {
    template: TPL.tools_options, 
    restrict: 'A', 
    replace:true, 
    scope: false,
    link: function($scope, el) {
    }}
}])
.directive('loginPop', ['FTPL', 'dragSvc', function(FTPL, dragSvc) {
    return {
        restrict: 'A',
        replace: false,
        template:   FTPL.popLogin,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('slideOut', [function() {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, el, attrs) {
            var xpos = 0;
            var ypos = 0;
            $scope.touchStart = function($event) {
                xpos = event.changedTouches[0].clientX;
                ypos = event.changedTouches[0].clientY;
            }

            $scope.touchEnd = function($event) {
                var movementX = xpos-event.changedTouches[0].clientX;
                var movementY = ypos-event.changedTouches[0].clientY;
                movementX > 50 && movementY < 50 && $scope.slideOut();
                return {movX: movementX, movY: movementY, swipeX: Math.abs(movementX) > 30,  swipeY: Math.abs(movementY) > 30 };
            }
        }
    }
}])
.directive('focusIt', [function() {
    return {
        restrict: 'A',
        replace:  false,
        link: function($scope, el, attrs) {
            var setFocus = function() { el[0].focus(); }

            $scope.setFocus = function() { setTimeout(setFocus, 100); }

            $scope.setFocus();
        }
    }
}])
.directive('dbadmin', ['FTPL', 'api', 'loginSvc', 'notifSvc', function(FTPL, api,loginSvc, notifSvc) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        template: FTPL.dbadmin,
        link: function($scope, el, attrs) {
            $scope.tabName = 'users';

            $scope.loadTab = function() {
                api.get( 'dbadmin/getTabDef', 'tab_name=' + $scope.tabName, function(resp) {
                    $scope.tabDef = resp.data;
                    notifSvc.info( 'Table ' +  $scope.tabName + ' loaded', 3);
                });
            }
            loginSvc.runAfterLogin( $scope.loadTab );

            $scope.dropCol = function(colName) {
                notifSvc.err('Delete column ' + colName + " ARE YOU REALLY SURE!!!", 10, function(resp) {
                    if (resp) {
                        notifSvc.warn( 'Deleting ...', 4);
                        api.get( 'dbadmin/dropCol', 'tab_name=' + $scope.tabName + '&col_name=' + colName , function(resp) {
                            $scope.loadTab();
                        });
                    } else
                        notifSvc.info( 'Delete Aborted', 2);
                });
            }
            $scope.newCol = {};
            $scope.addCol = function() {
                INFO( 'add col');
                notifSvc.warn('Add column ' + $scope.newCol.name, 10, function(resp) {
                    if (resp) {
                        notifSvc.warn( 'Adding Column...', 4);

                        var data = {
                            tab_name: $scope.tabName,
                            col_name: $scope.newCol.name,
                            col_type: $scope.newCol.type,
                            default:  $scope.newCol.default,
                            radix:    $scope.newCol.radix,
                            scale:    $scope.newCol.scale
                        };

                        api.post( 'dbadmin/addCol', data, function(resp) {
                            $scope.loadTab();
                        });
                    } else {
                        notifSvc.info( 'Add Column Aborted', 2);
                    }
                });
            }

            $scope.createTab = function() {
                console.log( $scope.newTabName, ' new tab name' );

                api.get( 'dbadmin/createTab', 'tab_name=' + $scope.newTabName, function(resp) {
                    $scope.tabName = $scope.newTabName;
                    $scope.loadTab();
                });
            }
           
            $scope.dropTab = function() {
                notifSvc.warn( 'Drop Table ' + $scope.newTabName + ' ARE YOU REALLY SURE!!!', 4 ,function(resp) {
                    if (resp)
                        notifSvc.err( 'One more time...Drop Table ' + $scope.newTabName + ' ARE YOU REALLY SURE!!!', 4 ,function(resp2) {
                            if (resp2) 
                                api.get( 'dbadmin/dropTab', 'tab_name=' + $scope.newTabName, function(resp) { });
                        });
                });
            }
        }
    }
}])
.directive('pgr', ['FTPL', 'notifSvc', 'LS', '$compile', function(FTPL, notifySvc, LS, $compile) {
    return {
        restrict: 'A',
        replace: false,
        scope: false,
        link: function($scope, el, attrs) {
            el.append($compile(FTPL.pgr)($scope));

            $scope.nav.perPage  = typeof attrs.pgrRowCount != 'undefined' && parseInt(attrs.pgrRowCount) > 0 
                                ? parseInt(attrs.pgrRowCount) : 8;

            $scope.nav.pgrSelections = [2, 4, 6, 8, 10];

            var activeData      = [];
            var allData         = null;
            var pgrCb           = null;
            $scope.nav.currPage = 1;

            $scope.setPg = function(pg) {
                typeof pg == 'undefined' && (pg = $scope.nav.currPage);
                console.log( activeData.length, $scope.nav.perPage );
                var numPages = Math.ceil(activeData.length/$scope.nav.perPage);

                $scope.pages = [];
                for (var i=0; i<numPages; i++)
                    $scope.pages.push(i+1);

                pg<1        && (pg=1);
                pg>numPages && (pg=numPages);
                $scope.nav.currPage = pg;

                var selectedData = [];
                for (var i=$scope.nav.perPage*(pg-1); i<$scope.nav.perPage*pg; i++)
                    if (typeof activeData[i] == 'undefined') break;
                    else                                     selectedData.push(activeData[i]);

                console.info(selectedData , pgrCb);
                pgrCb( selectedData );
            }

            $scope.pgrAction = function(typ, sortO, col) {
                switch (typ) {
                    case 'init':
                        //allData = $scope[attrs.itemList];
                        allData = $scope[col];
                        console.log( allData, ' ALL DATA ');
                        activeData = angular.copy( allData );
                        
                        pgrCb = sortO; // HACK - naming convention - cb function back to lst
                        $scope.setPg(1);
                        return allData;
                        break;
                    case 'sort':
                        activeData = activeData.sort(function(a, b) {
                            a[col] == null && (a[col] = '');
                            var dir = sortO[col] ? 1 : -1; //direction
                            return a[col] > b[col] ? -dir : dir;
                        });
                        //$scope.setPg($scope.nav.currPage);
                        $scope.setPg(1);
                        break;
                    case 'search':
                        activeData = sortO; // data filtered in lst
                        $scope.setPg();
                        break;
                }
            }
        }
    }
}])
.directive('lst', ['FTPL', 'notifSvc', 'LS', function(FTPL, notifySvc, LS) {
    return {
        restrict: 'AC',
        replace: false,
        template: FTPL.lst,
        scope: {itemList: "=", parentObj: "=", lstCb: "&", config: "="},
        link: function($scope, el, attrs) {
            console.log( 'config in lst', $scope.config );
            $scope.searchItems = {};
            $scope.hasAdd = typeof attrs.noAdd == 'undefined';
            $scope.filt   = typeof attrs.filt  != 'undefined';
            $scope.dataClass = attrs.dtClass || '';

            var hasPager = typeof attrs.pgr != 'undefined';

            $scope.noDel = typeof attrs.noDel != 'undefined';
            $scope.delIcon = attrs.delIcon || 'close';
            $scope.filterOn = typeof attrs.filterOn != 'undefined';

            $scope.cols = JSON.parse(attrs.cols.replace(/''/g,'S_QUOT')
                                               .replace(/'/g, '"')
                                               .replace(/S_QUOT/g,"'"));

            function pgUpdater(pgData) {
               $scope.iList = pgData;
            }

            $scope.$watch('itemList', function(n, o){
                if (typeof n != 'undefined') {
                    $scope.iList = angular.copy(n);
                    hasPager && ($scope.$parent.pgrAction('init', pgUpdater));
                }
            });

            $scope.listNav = {selRow: -1};
            setTimeout ( function() {
                var filt1 = el.find('input')[0];
                typeof filt1 == 'undefined'  || filt1.focus();
            }, 100);

            $scope.sortO =  {};
            for (var i in $scope.cols) $scope.sortO[$scope.cols[i][0]] = true;

            var parCols = $scope.cols.filter(function(c) { return typeof c[3] != 'undefined'; })
                                     .map(   function(c) { return { col: c[0], pCol: c[3] };  });

            function popParent(n) {
                typeof n == 'undefined' ||
                    parCols.map(function(c) { $scope.searchItems[c.col] = n[c.pCol]; });
            }

            typeof attrs.parentObj == 'undefined' ||
                $scope.$watch('parentObj', function(n, o){
                    typeof n == 'undefined' || popParent(n);
                });


            $scope.sortCol = function(col) {
                $scope.sortColName = col[0];

                $scope.sortO[col[0]] = !$scope.sortO[col[0]];
                if (hasPager) {
                    $scope.$parent.pgrAction('sort', $scope.sortO, $scope.sortColName);
                } else {
                    $scope.iList = $scope.iList.sort(function(a, b) {
                        a[col[0]] == null && (a[col[0]] = '');
                        var dir = $scope.sortO[col[0]] ? 1 : -1; //direction
                        return a[col[0]] > b[col[0]] ? -dir : dir;
                    });
                }
            }

            typeof LS('filter.andOr') == 'undefined' &&  LS('filter.andOr', false);

            $scope.isAnd = true;
            $scope.andOr = function() {
                if (LS('filter.andOr')) {
                    $scope.isAnd = !$scope.isAnd;
                    $scope.searchCol();
                }
            }

            $scope.searchCol = function() {
                var iList = angular.copy($scope.itemList);

                var iL = [];
                var uids=[];
                var hasSearch = false;
                for (var i in $scope.searchItems)
                    if ($scope.searchItems[i]) {
                        hasSearch = true;
                        break;
                    }

                if (hasSearch) {
                    for (var key in $scope.searchItems) {
                        var searchVal = $scope.searchItems[key].toUpperCase();
                        var searchLen = searchVal.length;

                        if ($scope.isAnd) {
                            iL = iList.filter( function(i) {
                                return i[key] && i[key].toString().toUpperCase().substring(0, searchLen) == searchVal;
                            });
                         } else {
                            iList.filter( function(i) {
                                if (uids.indexOf(i.uid) == -1)
                                    if (searchLen > 0 && i[key] && i[key].toString().toUpperCase().substring(0, searchLen) == searchVal) {
                                        uids.push(i.uid);
                                        iL.push(i);
                                    }
                                return false;
                            });
                        }
                    }
                    if (hasPager)   $scope.$parent.pgrAction('search', iL);
                    else            $scope.iList = iL;
                } else {
                    $scope.$parent.pgrAction('search', iList);
                }
            }

            $scope.clearSearch = function() {
                for (var i in $scope.searchItems)
                    $scope.searchItems[i] = '';

                if (hasPager) 
                    $scope.$parent.pgrAction('search', $scope.itemList);
                else
                    $scope.iList = $scope.itemList;
            }

            $scope.viewItem = function(item){
                $scope.lstCb({typ: 'view', item: item});
            }

            $scope.saveItem = function(item) {
                $scope.lstCb({typ: 'save', item: $scope.searchItems});
            }

            $scope.delItem = function(item, $event) {
                $event.stopPropagation();
                notifySvc.warn('Delete item?', 10, function(resp) {
                    if (resp)
                        $scope.lstCb({typ: 'del', item: item});
                });
            }

            $scope.editItem = function(item) {
                $scope.lstCb({typ: 'edit', item: item});
            }

        }
    }
}])
.directive('cmsLink', ['FTPL', 'dragSvc', function(FTPL, dragSvc) { return {
    template: FTPL.cms_link, 
    restrict: 'A', 
    replace:true, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        typeof attrs.cmsLink == 'string' && ($scope.idxA = attrs.cmsLink.split(':'));

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, FTPL.cms_link_pop, {cls: 'linkPop'});
        }
    }}
}])
.directive('popDragDepr', ['FTPL', function(FTPL) { return {
    restrict: 'A', 
    //replace:true, 
    scope: false,
    compile: function(el, attrs) {
        console.log( 'compile popDrag', el);
        return function($scope) {
            console.log( 'popDrag linker');
            $scope.popMe = function( arg ) {
                console.log( 'pop me', arg);
            }
        }}
    }
}])
.directive('cmsLinkText', ['FTPL', 'dragSvc', function(FTPL, dragSvc) { return {
    template: FTPL.cms_linkText, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.inLink = false;
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        var contEl = el.find('content');

        setTimeout( function() {
            $scope.richCont = $scope.cont;
            $scope.$$phase || $scope.$digest();
        }, 100);

        $scope.$watch('richCont', function(n, o) {
            typeof n == 'undefined' || (contEl[0].innerHTML = $scope.cont = $scope.richCont);
        });

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            $scope.cb({edtme: $scope.edtme});
            dragSvc.init($scope, el, FTPL.cms_linkText_pop);
        }

        var lT;
        var pos = 0;
        $scope.mkLink = function() {
            $scope.inLink = !$scope.inLink;

            lT = el.find('linkText').find('div')[0];
            function handler(ev) {
                console.log( window.getSelection(), ' sel ' );
                pos = window.getSelection().baseOffset;

            };

            lT.addEventListener('keyup',  handler);
            lT.addEventListener('click',  handler);
        }

        $scope.linkLabel = 'GO THERE';
        $scope.linkUrl = 'http://enkoda.ca';

        $scope.insertLink = function(lab, url) {
            //var pos = el.find('textarea').prop('selectionStart');

            var link = '<a href="' + url + '">' + lab + '</a>';
            $scope.cont = $scope.cont.substring(0, pos) + link + $scope.cont.substring(pos);
        }
        $scope.trans = function() { 
            lT.innerHTML = $scope.cont;
        }

        $scope.transBack = function() { 
            $scope.cont = lT.innerHTML;
        }


        setTimeout( function() {
        }, 1000);

        $scope.abc = 'abc';
        $scope.mouseUp = function() {
            console.log( 'mouseup');
            //el.find('linkText').find('div')[0].innerHTML = $scope.cont;
        }
    }}
}])
.directive('cmsInText', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.cms_inText, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;
        var spanEl = el.find('content').find('span');

        var shadow;
        var unreg = $scope.$watch('cont', function(n, o) {
            if (typeof n != 'undefined') {
                shadow = n;
                unreg();
            }
        });

        $scope.pencilClick = function() {
            console.log( $scope.edtme,'  edt me ' );
            if ($scope.edtme) {
                $scope.cont = spanEl[0].innerText;
                shadow == $scope.cont || setTimeout( function() {
                    $scope.cb({arg: $scope.saveText});
                    shadow = $scope.cont;
                }, 100);
            } else {
                setTimeout( function() {
                   spanEl[0].focus(); 
                }, 100);
            }
            $scope.edtme = !$scope.edtme;
        }

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.saveText = function(sucs) {
            if (sucs) {
                //$scope.edtme = !$scope.edtme;
                notifSvc.ok('Saved', 1);
                //$scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsText', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.cms_text, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            //$scope.cb({edtme: $scope.edtme});
            dragSvc.init($scope, el, FTPL.cms_text_pop);
        }

        $scope.saveText = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved', 2);
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsLocationInfo', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.contact_locationInfo, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            //$scope.cb({edtme: $scope.edtme});

            dragSvc.init($scope, el, FTPL.contact_locationInfo_pop);
        }

        $scope.saveLocInfo = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsMenuItem', ['FTPL', 'dragSvc','notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.product_menuItem, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&", cmsData: "="},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
       }

        $scope.pencilClick = function(idx, content) {
            $scope.content = content;
            $scope.idx     = idx;

            console.log(content, ' cont ', idx, ' idx ');

            $scope.edtme = !$scope.edtme;
            //$scope.cb({edtme: $scope.edtme});

            dragSvc.init($scope, el, FTPL.product_menuItem_pop);
        }

        $scope.saveResp = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.delItem = function() {
            console.log( $scope.cont, $scope.idx );
            notifSvc.warn("Permanently delete menu item?", 5, function(yN) {
                if (yN) {
                    $scope.cont.splice($scope.idx, 1);
                    $scope.cb({arg: $scope.delItemCb});
                }
            });
        }

        $scope.delItemCb = function() {
            console.log('deleted');
            $scope.pencilClick();
        }

        $scope.addItem = function(catIdx) {
            $scope.cont.push({name:'', price:'', descr:'', hippie: ''});
            $scope.content = $scope.cont[$scope.cont.length -1];

            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, FTPL.product_menuItem_pop);
        }

        $scope.newMenuItem = function(sucs) {
            console.log(' created ', sucs);
        }

        $scope.saveOrderCb = function() {
            console.log('order saved');
            $scope.pencilClick();
        }

        $scope.saveOrder = function() {
            $scope.cb({arg: $scope.saveOrderCb});
        }

        $scope.reorder = function(list) {
            $scope.orderSubList = list;
            console.log('orderSubList ', $scope.orderSubList);
            var reorderTpl = '<div ng-if="reorder" reorder order-list="orderSubList" handle="name" save-order="saveOrder(arg)"></div>';
            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, reorderTpl, 'topRight');
        }
    }}
}])
.directive('cmsMenuCat', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.product_menuCat, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&", nav: "=", prodIdx: "="},
    //scope:false,
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            //$scope.cb({edtme: $scope.edtme});

            dragSvc.init($scope, el, FTPL.product_menuCat_pop);
        }
                
        $scope.saveReorder = function() {
            $scope.inReorder || $scope.cb({arg: $scope.reorderMenuCat});
        }

        $scope.reorderMenuCat = function(sucs) {
            if (sucs) {
                notifSvc.ok('New Order Saved');
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.saveCat = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.reorder = function() {
            var reorderTpl = '<div ng-if="reorder" reorder order-list="cont" handle="lab" save-order="saveOrder()"></div>';
            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, reorderTpl, {anchorRight: true});
        }

        $scope.addCat = function(idx) {
            if (typeof idx == 'undefined') {
                notifSvc.warn('Please select location', 3); 
            } else {
                /*console.log( idx, ' prod idx', $scope.cms.locs[idx] );*/

                if (typeof $scope.cont  == 'undefined')
                    $scope.cont = [];

                $scope.cont.push({lab:'', items:[]});
                //$scope.pencilClick();
            }
        }

        $scope.delCat = function(i) {
            if ( $scope.cont[i].lab == '' ) {
                $scope.cont.splice(i ,1);
            } else {
                notifSvc.warn("Permanently delete category " + $scope.cont[i].lab + "?", 5, function(yN) {
                    if (yN) {
                        $scope.cont.splice(i ,1);
                        $scope.cb({arg: $scope.delLinkSec});
                    }
                });
            }
        }

        $scope.delLinkSec = function(sucs) {
            if (sucs) {
                notifSvc.ok('Link Deleted');
                $scope.inReorder = false;
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsLinkSection', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.cms_linkSection, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.opts = {
            containment: 'div'
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, FTPL.cms_linkSection_pop, {cls: 'popHours'});
        }

        $scope.saveLinkSec = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.saveReorder = function() {
            $scope.inReorder || $scope.cb({arg: $scope.saveReorderSec});
        }

        $scope.saveReorderSec = function(sucs) {
            if (sucs) {
                notifSvc.ok('New Order Saved');
                $scope.inReorder = false;
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.addLink = function(i) {
            $scope.cont.links.push({});
        }

        $scope.delLink = function(i) {
            if ( typeof $scope.cont.links[i].linkText == 'undefined' ) {
                $scope.cont.links.splice(i ,1);
            } else {
                notifSvc.warn("Permanently delete link '" + $scope.cont.links[i].linkText + "'" , 5, function(yN) {
                    if (yN) {
                        $scope.cont.links.splice(i ,1);
                        $scope.cb({arg: $scope.delLinkSec});
                    }
                });
            }
        }

        $scope.delLinkSec = function(sucs) {
            if (sucs) {
                notifSvc.ok('Link Deleted');
                $scope.inReorder = false;
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsHours', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.contact_hours, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&"},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            //$scope.cb({edtme: $scope.edtme});
            dragSvc.init($scope, el, FTPL.contact_hours_pop, {cls: 'popHours'});
        }

        $scope.saveHours = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }
    }}
}])
.directive('cmsSectionText', ['FTPL', 'dragSvc', 'notifSvc', function(FTPL, dragSvc, notifSvc) { return {
    template: FTPL.cms_sectionText, 
    restrict: 'A', 
    replace:false, 
    scope: {cont: "=", edt: "=", cb: "&", opts:"="},
    link: function($scope, el, attrs) {
        $scope.placeholder = attrs.cmsText
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;
            $scope.cb({edtme: $scope.edtme});

            dragSvc.init($scope, el, FTPL.cms_sectionText_pop);
        }

        $scope.saveText = function(sucs) {
            if (sucs) {
                notifSvc.ok('Saved');
                $scope.pencilClick();
            }
            else {
                notifSvc.warn('Something went wrong');
            }
        }

        $scope.blogLinkChanged = function() {
            console.log( $scope.blogLink, typeof $scope.blogLink );

            $scope.cont.link        = JSON.parse($scope.blogLink).url;
            $scope.cont.linkText    = JSON.parse($scope.blogLink).text;
        }
    }}
}])
.directive('cmsVid', ['FTPL', 'dragSvc', '$sce', function(FTPL, dragSvc, $sce) { return {
    //template: TPL.tools_cmsImg, 
    template: FTPL.cms_vid, 
    restrict: 'A', 
    replace:true, 
    scope: {vidSrc: "=", edt: "=", cb: "&", vidList: "="},
    link: function($scope, el, attrs) {
        $scope.trustVid = function(src) { return $sce.trustAsResourceUrl(src); }
    }}
}])
.directive('cmsImg', ['FTPL', 'dragSvc', '$compile', function(FTPL, dragSvc, $compile) { return {
    //template: TPL.tools_cmsImg, 
    template: FTPL.cms_bg, 
    restrict: 'A', 
    replace:true, 
    scope: {imgSrc: "=", edt: "=", cb: "&", imgList: "="},
    link: function($scope, el, attrs) {

        $scope.pencilClick = function(action) {
            typeof action == 'undefined' && (action = 'clicked');
            $scope.edtme = !$scope.edtme;
            dragSvc.init($scope, el, FTPL.cms_bg_pop, true /* overTop */);
            typeof $scope.cb == 'function' && $scope.cb({action: action, val: $scope.edtme});
        }

        $scope.contain = typeof attrs.contain == 'undefined' ? 'cover' : 'contain';
        $scope.showSave = typeof attrs.showSave != 'undefined';

        $scope.multiChoice = typeof attrs.multiChoice != 'undefined';
        $scope.lab = typeof attrs.lab == 'undefined' ? '' : attrs.lab;

        $scope.setPath = function(file, grp, owner_uid) {
            console.log( ' path in cms img', file,grp, owner_uid, file.path);
            //$scope.imgSrc = 'files/' + owner_uid + '/' + grp + '/' + file.name;
            $scope.imgSrc = file.path;
        }

        $scope.save = function() {
            $scope.cb({action: 'save'});
        }

        $scope.hidePop = function() {
            $scope.edtme = false;
        }

        if ( false && typeof attrs.maxWidth != 'undefined') {
            var img = new Image();
            img.src = 'https://cms.enkoda.ca/' + $scope.imgSrc;
            img.onload = function() {
                console.log( angular.element(img) );
                console.log( img.width );
                var dims = {h: img.height, w: img.width};
                var maxWidth = parseInt( attrs.maxWidth );
                if (dims.w > maxWidth) {
                    dims.h = maxWidth * (dims.h/dims.w)
                    dims.w = maxWidth;
                } else {
                    //dims = {w:360, h:360};
                }

                $scope.dims = dims;
                console.log( $scope.dims, ' dims ');
            }
        }
    }}
}])
.directive('cmsImgSideSlide', ['FTPL', 'dragSvc', function(FTPL, dragSvc) { return {
    template: FTPL.cms_imgSideSlide, 
    restrict: 'A', 
    replace:false, 
    scope: {edt: "=", cb: "&", imgList: "=", loopPeriod: '='},

   link: function($scope, el, attrs) {
        $scope.pencilClick = function() {
            $scope.edtme = !$scope.edtme;

            $scope.cb({edtme: $scope.edtme});
            dragSvc.init($scope, el, FTPL.cms_imgSideSlide_pop, true);
        }
        $scope.multiChoice = true;
        
        $scope.byImg = typeof attrs.byImg != 'undefined';


        var slides = [];
        var mod;
        var slidesDom;
        var transTm = 1200;

        $scope.setPath = function(file) {
            console.log( 'set path called in cmsimgsideslide');
            //setLoop(true);

            $scope.imgSrc = 'files/' + file.owner_uid + '/' + file.name;
        }

        $scope.idx = -1;
        var lastTs=new Date().getTime();
        function jump() {
            if (stopSlide) return;

            if (new Date().getTime()-lastTs< transTm*0.8) return false;
            lastTs=new Date().getTime();

            var nextIdx = (idx+1)%mod;
            slides[nextIdx].css('zIndex', '3').css('transform', 'translateX(0)');
            slides[idx].css('zIndex', '2').css('transform', 'translateX(-100%)');

            var ii = idx;
            setTimeout(function() {
                slides[ii].css('zIndex', '1').css('transform', 'translateX(100%)');

                //console.log( 'ii', ii, idx, slides[idx] );
            }, transTm+100);
            idx = nextIdx;
        }

        $scope.$watch('imgList', function(n, o) {
            slides = [];
            setTimeout( function() {
                n && setLoop(true);
                $scope.$$phase || $scope.$digest();
            }, 100);
        });

        var stopSlide  = false;
        $scope.stopSlide = function() {
            stopSlide = !stopSlide;
            console.log( 'stop slide', stopSlide );
        }

        function moveRight(exIdx) {
            slidesDom.css('transition','0');
            slides.map(function(m, i) { 
                i==exIdx || m.css('transform', 'translateX(100%)').css('zIndex', '1');; 
            }); 
            slides[exIdx].css('transform', 'translateX(0)').css('zIndex', '3'); 
            setTimeout( function() {slidesDom.css('transition','transform ' + transTm + 'ms ease, z-index 1ms'  );}, $scope.loopPeriod*900);
        }

        $scope.jump = function() {
            runLoop(true);
            jump();
        }

        var loopHandle = null;
        function runLoop(restart) {
            restart && clearInterval(loopHandle);
            loopHandle = setInterval( function() {
                jump();
                $scope.$$phase || $scope.$digest();
            }, parseInt($scope.loopPeriod)*1000);
        }

        var idx = 0;
        function setLoop(isRestart) {
            $scope.current = Object.keys($scope.imgList)[0];

            slidesDom = el.find('slide');

            for (var i=0; i<slidesDom.length; i++) {
                var ss = angular.element(slidesDom[i]);
                slides[i] = angular.element(ss);
            }
            moveRight(0);
            mod = slides.length;
            runLoop(typeof isRestart == 'undefined' ? false : isRestart);
        }

        var unregWatch  = $scope.$watch('loopPeriod', function(n, o) {
            if (typeof n != 'undefined' && parseInt(n) > 0) {
                //setTimeout( setLoop, 10);
                unregWatch();
            }
        });
    }}
}])
.directive('cmsImgSlide', ['FTPL', function(FTPL) { return {
    template: FTPL.cms_imgSlide, 
    restrict: 'A', 
    replace:false, 
    scope: {edt: "=", cb: "&", imgList: "=", loopPeriod: '='},
    link: function($scope, el, attrs) {
        $scope.multiChoice = true;
        $scope.setPath = function(file) {
            $scope.imgSrc = 'files/' + file.owner_uid + '/' + file.name;
        }

        var loopList = [];
        var loop = 1;
        var unreg = $scope.$watch('imgList', function(n, o) {
            if (typeof n != 'undefined') {
                loopList = Object.keys($scope.imgList);
                loop = loopList.length;
                $scope.current = loopList[0];
                unreg();
            }
        });

        var idx = 0;
        $scope.jump = function() {
            $scope.current = loopList[(++idx)%loop];
        }

        var unreg2 = $scope.$watch('loopPeriod', function(n, o) {
            if (typeof n != 'undefined' && parseInt(n) != 0) {
                setInterval( function() {
                    loopList = Object.keys($scope.imgList);
                    loop = loopList.length;
                    $scope.current = loopList[(++idx)%loop];
                    $scope.$$phase || $scope.$digest();
                }, parseInt($scope.loopPeriod)*1000);
                unreg2();
            }
        });
    }}
}])
.directive('contact', ['FTPL', 'notifSvc', 'loginSvc', function(FTPL, notifSvc, loginSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.contact,
        link: function($scope, el, attrs) {
            setTimeout( function() {
                console.log( $scope.cms );
                $scope.mapCoords = $scope.cms.locs[0];
                $scope.$$phase || $scope.$digest();
            }, 400);

            //$scope.mapCoords = $scope.cms.milCoords;
            //$scope.mapLab = $scope.cms.milLab;

            $scope.selectMap = function(arg) {
                console.log( arg );
                $scope.mapCoords = $scope.cms.locs[arg];
            }
        }
    }
}])
;
//var module = angular.module("lvl.directives.dragdrop", ['lvl.services']);

APP
.factory('uuid', function() {
    var svc = {
        new: function() {
            function _p8(s) {
                var p = (Math.random().toString(16)+"000000000").substr(2,8);
                return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
            }
            return _p8() + _p8(true) + _p8(true) + _p8();
        },

        empty: function() {
          return '00000000-0000-0000-0000-000000000000';
        }
    };

    return svc;
})
.directive('lvlDraggable', ['$rootScope', 'uuid', function($rootScope, uuid) {
    return {
        restrict: 'A',
        link: function(scope, el, attrs, controller) {
            console.log("linking draggable element");

            angular.element(el).attr("draggable", "true");
            var id = attrs.id;
            if (!attrs.id) {
                id = uuid.new()
                angular.element(el).attr("id", id);
            }

            el.bind("dragstart", function(e) {
                e.dataTransfer.setData('text', id);

                $rootScope.$emit("LVL-DRAG-START");
            });

            el.bind("dragend", function(e) {
                $rootScope.$emit("LVL-DRAG-END");
            });
        }
    }
}])
.directive('lvlDropTarget', ['$rootScope', 'uuid', function($rootScope, uuid) {
        return {
            restrict: 'A',
            scope: {
                onDrop: '&'
            },
            link: function(scope, el, attrs, controller) {
                var id = attrs.id;
                if (!attrs.id) {
                    id = uuid.new()
                    angular.element(el).attr("id", id);
                }

                el.bind("dragover", function(e) {
                  if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                  }

                  e.dataTransfer.dropEffect = 'move';  // See the section on the DataTransfer object.
                  return false;
                });

                el.bind("dragenter", function(e) {
                  // this / e.target is the current hover target.
                  angular.element(e.target).addClass('lvl-over');
                });

                el.bind("dragleave", function(e) {
                  angular.element(e.target).removeClass('lvl-over');  // this / e.target is previous target element.
                });

                el.bind("drop", function(e) {
                  if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                  }

                  if (e.stopPropogation) {
                    e.stopPropogation(); // Necessary. Allows us to drop.
                  }
                    var data = e.dataTransfer.getData("text");
                    var dest = document.getElementById(id);
                    var src = document.getElementById(data);
                    console.log( 'dropped', src, dest );

                    //scope.onDrop({dragEl: src, dropEl: dest});
                    scope.onDrop({dragEl: 1, dropEl: 2});
                });

                $rootScope.$on("LVL-DRAG-START", function() {
                    var el = document.getElementById(id);
                    angular.element(el).addClass("lvl-target");
                });

                $rootScope.$on("LVL-DRAG-END", function() {
                    var el = document.getElementById(id);
                    angular.element(el).removeClass("lvl-target");
                    angular.element(el).removeClass("lvl-over");
                });
            }
        }
}]);
;
