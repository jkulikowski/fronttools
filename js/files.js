APP
.factory('fileSvc', ['api', function(api) {
    var files = null;
    var cbs = [];

    function remapFiles(files) {
        files.all.map( function(f) {
            f.path = 'files/' + f.path;
        });

        for (var i in files.grp) {
            files.grp[i].map(function(f) {
                f.path = 'files/' + f.path;
            });
        }
        return files;
    }

    function loadFiles(cb) {
        api.get('files/getDirs', function(resp) {
            files = resp.data;
            remapFiles( files );

            cbs.map(function(cb) {
                typeof cb == 'function' && cb(files);
            });
            typeof cb == 'function' && cb(files);
        });
    }
    return { 
        loadFiles: loadFiles,
        remapFiles: remapFiles,
        getFiles: function() { return files; },
        reg: function(cb) { 
            files == null ?  cbs.push(cb) : cb(files); 
        }
    };
}])
.directive('files', ['FTPL', 'notifSvc', 'fileSvc', 'api', function(FTPL, notifSvc, fileSvc, api) {
    return {
        restrict: 'A',
        replace: false,
        template: FTPL.files,
        link: function($scope, el, attrs) {
            $scope.groupFiles = true;

            function findInList(file) {
                $scope.imgList.filter( function(m) {
                    console.log( 'ZZZ', file, m);
                });
            }

            $scope.removeChoice = function(file) {
                $scope.imgList = $scope.imgList.filter(function(f) {
                    return f.file != file.file;
                });
            }

            $scope.chooseFile = function(file, $event) {
                if (typeof $scope.imgList == 'undefined') {
                    $scope.setPath( file , $scope.nav.selGrp, $scope.owner_uid); 
                } else {
                    if (typeof $scope.imgList[file.uid] == 'undefined') {
                        $scope.imgList.push({file: file.path});
                        typeof $scope.setPath == 'undefined' || $scope.setPath( file );
                        console.info( $scope.imgList , ' img list' );
                    } else {
                        delete $scope.imgList[file.uid];
                    }
                }
                console.info( $scope.imgList, ' img list ' );
            }

            $scope.createGrp = function() {
                if ($scope.nav.newGrp != 'undefined' && $scope.nav.newGrp) {
                    $scope.files[$scope.nav.newGrp] = [];
                    $scope.nav.selGrp = $scope.nav.newGrp;
                    notifSvc.info('Please upload file to "' + $scope.nav.newGrp + '"', 5);
                    $scope.newUpload = true;
                }
                $scope.nav.addGrp = false;
                $scope.nav.newGrp = '';
            }

            $scope.upDone = function() { 
                fileSvc.loadFiles(function(files) {
                    console.info( ' after up file ', files, $scope.nav.selGrp );
                    $scope.files = files.grp;
                }); 
            }

            typeof $scope.nav == 'undefined' && ($scope.nav = {});

            function loadedFiles(files) {
                $scope.owner_uid = files.owner_uid;

                $scope.files = $scope.groupFiles ? files.grp : files.all;
                $scope.groupFiles && 
                    ($scope.nav.selGrp = Object.keys($scope.files)[0]);
            }

            fileSvc.reg(loadedFiles);

            $scope.delImg = function(uid) {
                notifSvc.warn('This image will be deleted. Please confirm', 5, function(yN) {
                    yN && api.del('files/delFile/' + uid, [], null,  function(resp) {
                        $scope.files = fileSvc.remapFiles(resp.data).grp;
                    });
                })
            }
        }
    }
}])
;
