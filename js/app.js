if (typeof APP == 'undefined') var APP = angular.module('FRAME',['ngFileUpload', "ng.deviceDetector", "ngTouch", "ngRoute"]);

function waitForDom(domPar, domEl, fn, cnt) {
    typeof cnt == 'undefined' && (cnt=40);
    if (cnt > 0) 
        if (domPar[domEl]) fn(); 
        else setTimeout(function() {waitForDom(domPar, domEl, fn, --cnt);}, 100);
}

window.addEventListener("offline", function(e) { console.info("offline >>>>>>>>>>>"); });
window.addEventListener("online", function(e) { console.info("online >>>>>>>>>>"); });

function isOnline(yes, no){
    var xhr = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHttp');
    xhr.onload = function(){
        if(yes instanceof Function){
            yes();
        }
    }
    xhr.onerror = function(){
        if(no instanceof Function){
            no();
        }
    }

    xhr.open("GET","activities/notifsFor/?uid=874cc26e-52d7-1d4a-c8d4-6966e2666ad3",true);
    xhr.send();
}

function runSocket() {
    sockjs = new SockJS('/data');

    sockjs.onopen    = function()  {
        console.log('open' +  sockjs.protocol);
        restarts = 1;
        setTimeout( function() { sockjs.send(''); }, 30);
    };

    sockjs.onmessage = function(e) {
        console.log('from SOCKET', JSON.parse(e.data));
    };

    sockjs.onclose   = function()  {
        console.log('close...restarting ', restarts);
        restarts<=10 && setTimeout(function() { runSocket(); }, 3000*(restarts++));
    };
}

//runSocket();


/*
setTimeout( function() {
    var cont = document.getElementById('bottomUp');
    cont.addEventListener('touchstart', function(e) {
    });
    cont.addEventListener('touchend', function(e) {
        //cont.style.background='red';
        alert('touchend');
    });
}, 1000);
*/
/*
function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      if (req.status == 200) {
        resolve(req.response);
      }
      else {
        reject(Error(req.statusText));
      }
    };

    req.onerror = function() {
      reject(Error("Network Error"));
    };

    req.send();
  });
};


get('js/json.json').then(JSON.parse)
.then(function(arg) { return arg.parent; })
.then(function(arg) { return arg.child;  })
.then(function(arg) { return arg.name; })
.then(function(arg) { console.log( '5', arg ); });


isOnline(function(yes) {
    console.log( 'online' );
}, 
function(no) {
    console.log( 'not online' );
});
*/

/*
window.onfblogin = function() {
    console.log( arguments, ' fb login on login ' );
}

  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log('RESPONSE>>>', response);
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    FB.login();
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
        console.log( ' GLS ', response );
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '279916378773170',
    cookie     : true,  // enable cookies to allow the server to access
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.5' // use graph api version 2.5
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
    console.log( 'RESPONSE FB;', response, ' response ' );
      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
  }
*/

//var socket = new Websocket('./socket/get');
/*
var socket = new WebSocket("ws://pitfield.webroad66.com/socket", 'echo-protocol');

socket.onmessage = function(evt) {
    var msg = evt.data;
    console.log( 'onmessage', msg);
    //document.getElementById('dump').innerHTML=msg;
    console.log( "MSG", msg );
}

socket.onopen = function(data) {
    console.log('OPEN', data, data.data);
    socket.send('["data in array"]');
};

socket.onerror = function(err) {
    console.log( err );
}
*/
