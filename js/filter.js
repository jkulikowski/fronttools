APP
.factory('lngSvc', [function() {
    var conv = {
        'bar':      'barre',
        'line':     'ligne',
        'step':     'marche',
        'hist':     'histogramme',
        'area':     'plein',
        'scat':     'point',
        'Users':    'Utilisateurs',
        'Auth':     'Rang',
        'First':    'Prénom',
        'Last':     'Nom',
        'name':     'Nom',
        'Phone':    '# cellulaire',
        'Deleted':  'Supprimé',
        'Zones':    'Zones',
        'Logins':   'Accès',
        'Db Name':  'Nom de la base',
        'Login':    'Identifiants',
        'Pass':     'Mot de passe',
        'Perms':    'Rang d`accès',
        'auth_flag':'Rang',
        'first':    'Prénom',
        'last':     'Nom',
        'phone':    '# cellulaire',
        'Dashboard':        'Tableau de bord',
        'User Data':        'Utilisateurs',
        'Infrastructure':   'Infrastructures',
        'Zone rules':       'Contrôle de zone',
        'Notifications':    'Notifications',
        'Welcome':          'Bienvenue',
        'Logout':           'Déconnexion',
        'Username':         'Identifiant',
        'Password':         'Mot de passe',
        'Login':            'Connexion',
        'Refresh':          'Rafraichissement',
        'seconds':          'secondes',
        'Registered':       'U. Enregistrés',
        'Chart type':       'Type de graph',
        'Add User for':     'Ajouter un accès pour',
        'Saving...':        'enregistrement...',
        'Login Name':       'Identifiant',
        'Create new login': 'Ajouter',
        'Create':           'Ajouter',
        'Click to confirm': 'Cliquez pour confirmer',
        'is now monitoring':'est surveillant de la zone',
        'Restricted employees':     'Utilisateurs exclus',
        'Welcome to Webroad 66':    'Bienvenue dans Webroad66',
        'Current Count per Beacon': 'Nombre d`utilisateurs par zone',
        'Client Logins for':        'Accès administratifs à la console',
        'Please select a Zone from the left pane': 'Sélectionnez une zone dans le panneau de gauche',
        'Restricted employees selected': '"Utilisateurs exclus" sélectionné',
        'Zone Vigilants':   'Surveillants de zone',
        'Zone Vigilants selected':  '"Surveillants de zone" sélectionné',
        'will now be monitored in': 'n`est pas autorisé dans la zone',
        'is no longer monitored in': 'est autorisé dans la zone',
        'is no longer monitoring in': 'n`est plus surveillant de la zone'
    }
    return conv;
}])
.filter('fheads', ['lngSvc', function(lngSvc) {
    return function(input, lng) {
        var conv = lngSvc[input];
        if (typeof lngSvc[input] != 'undefined') 
            (typeof lng == 'undefined' || lng == 'fr') && (input = lngSvc[input]);
        input.indexOf('flag') > -1 && (input = input.substring(0, input.length-5));
        var under = input.toLowerCase().replace(/_(.)/g, function(match, group1) {
            return ' ' + group1.toUpperCase();
        });
        return under.substring(0,1).toUpperCase() + under.substring(1);
    }
}])
.filter('alterRow', function() {
    return function(input, arg, skip) {
        input -= arg-1;
        if (typeof skip == 'undefined') skip = 2;
        return Math.floor(input/skip - 0.001) % 2 ? '' : 'alter';
    };
})
.filter('parseInt', function() {
    return function(input) { return parseInt(input); }
})
.filter('checkActive', function() {
    return function(input, altClassName) { 
        altClassName = typeof altClassName == 'undefined' ? 'active' : altClassName;
        return input ? altClassName : ''; 
    };
})
.filter('checkInactive', function() {
    return function(input) { return input ? 'inactive' : ''; };
})
.filter('pushBack', function() {
    return function(input) {
        return input ? 'inactive' : '';
    };
})
.filter('threeWay', function() {
    return function(input) {
        //return input < 0 || input > 2 ? 'noshow' : ['noshow', 'down', 'up'][input];
        var ret;
        switch(input) {
            case 1:     ret = "down";   break;
            case 2:     ret = "up";     break;
            default :   ret = "noshow"; break;
        }
        return ret;
    };
}) 
.filter('activeBlur', function() {
    return function(input,isStart) {
        var ret;
        isStart || (input = input*2%3);

        switch(parseInt(input)) {
            case 2:     ret = 'active'; break;
            case 1:     ret = 'active blur'; break;
            default:    ret = ''; break;
        }
        return ret;
    }
})
.filter('getClass', function() {
    return function(input) {
        var ret;
        switch(input) {
            case 'rssi':         ret = "small";     break;
            case 'zone_uid':     ret = "huge";      break;
            case 'zone_name':    ret = "large";     break;
            case 'macaddress':   ret = "large";     break;
            case 'uid':          ret = "huge";      break;
            case 'location_uid': ret = "huge";      break;
            case 'name':         ret = "large";     break;
            case 'id':           ret = "huge";      break;
            case 'did':          ret = "huge";      break;
            default :            ret = "";          break;
        }
        return ret;
    };
}) 
.filter('heads', function() {
    return function(input) {
        input.indexOf('flag') > -1 && (input = input.substring(0, input.length-5));
        var under = input.toLowerCase().replace(/_(.)/g, function(match, group1) {
            return ' ' + group1.toUpperCase();
        });
        return under.substring(0,1).toUpperCase() + under.substring(1);
    }
})
.filter('labClass', function() {
    return function(input) {
        return typeof input == 'string' && input.indexOf('flag') > -1 ? 'flag' : '';
    }
})
.filter('checkUp', function() {
    return function(input, list) {
        //return (list.length)/parseInt(input) < 1.1 ? 'up' : '';
        return (list.length)/parseInt(input) < 1.9 ? 'up' : '';
    }
})
.filter('dtFmt', function() {
    return function(input) {
        var dt = new Date(input).toString().split(' ');
        return dt[1] + ' ' + dt[2] + ', ' + dt[3];
    }
})
.filter('ucLc', function() {
    return function(input) {
        return  input;
        var ret = '';
        var parts = input.split(' ');
        for (var i=0; i<parts.length; i++)
            ret += ' ' + parts[i].charAt(0).charAt(0).toUpperCase() + parts[i].slice(1).toLowerCase();
        //return  ret.substring(1);
    }
})
;
