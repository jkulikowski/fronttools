window.REST = true;
APP
.directive('blogItem', ['FTPL', 'api', 'dragSvc', 'notifSvc', function(FTPL, api, dragSvc, notifSvc) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.blog_item,
        scope: false,
        scope: { itemList: "=", edt:"=" , blogId:"=", reload:"&"},
        link: function($scope, el) {
            typeof $scope.nav == 'undefined' ? $scope.nav = { addMode: false} : $scope.nav.addMode = false;

            $scope.pencilClick = function() {
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, FTPL.blog_item_pop);
            }

            $scope.save = function() {
                $scope.itemList.handle = $scope.blogId;
                api.post('blog/saveItem', $scope.itemList, function(blogResp) {
                    $scope.itemList.uid = blogResp.data[0].uid;
                });
            }

            $scope.delete = function() {
                notifSvc.warn('Delete this blog item?', 5, function(yN) {
                    yN && api.del('blog/delItem', 
                        'uid=' + $scope.itemList.uid + '&blog_uid=' + $scope.blogId, 
                        function(blogItemResp) {
                            $scope.reload({data: blogItemResp.data});
                    });
                });
            }

            $scope.imgCb = function(action) {
                console.info('imgCb itemlist', action);
                $scope.save();
            }

            $scope.$watch('itemList', function(n,o) {
                typeof n != 'undefined' && n.title == '' && $scope.pencilClick();
            });
       }
    }
}])
.directive('blogItems', ['FTPL', 'api', '$location', 'dragSvc', 'notifSvc', '$window',function(FTPL, api, $location, dragSvc, notifSvc, $window) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.blog_items,
        scope: false,
        link: function($scope, el) {
            typeof $scope.nav == 'undefined' ? $scope.nav = { addMode: false} : $scope.nav.addMode = false;
            $scope.blogId = $location.path().split('/')[2];

            $scope.pencilClickDepr = function() {
                $scope.edtme = !$scope.edtme;
                console.log( FTPL.blog_items_pop, 'zzz');
                dragSvc.init($scope, el, FTPL.blog_items_pop);
            }

            var reorderTpl = '<div ng-if="reorder" reorder order-list="itemList" handle="title" save-order="saveOrder()"></div>';
            $scope.reorder = function() {
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, reorderTpl, {topRight: true});
            }

            $scope.saveOrder = function() {
                var ord = $scope.itemList.map(function(b) { return b.uid; });

                api.post('blog/reorderItems', {uids: ord}, function(resp) {
                    notifSvc.ok('Blog Items were reordered', 3);
                    $scope.pencilClick();
                });
            }

            $scope.pencilClick = $scope.reorder;

            $scope.addItem = function(sub_sub) {
                console.log($scope.itemList, ' itemList' );
                if ( $scope.itemList.length && typeof $scope.itemList[0].uid == 'undefined' )
                    $scope.itemList.shift();
                else 
                    $scope.itemList.unshift( {title: '', img: './image/noimage.png', content: '', keywords: '', created: new Date()} );
            }

            $scope.reload = function(data) {
                console.info( ' in Item RELOAD ', data);
                if (typeof data == 'undefined') {
                    var url = !window.REST ? 'api/blog/getItem?handle=' + $scope.blogId :
                                'api/blog/getItem/' + $scope.blogId ;
                    api.get(url, function(blogItemResp) {
                        $scope.itemList = blogItemResp.data;
                    });
                } else 
                    $scope.itemList  = data;
            };

            $scope.reload();
       }
    }
}])
.directive('blogPost', ['FTPL', 'api','loginSvc', 'dragSvc','notifSvc', function(FTPL, api, loginSvc, dragSvc, notifSvc) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.blog_post,
        scope: { mainList: "=", edt:"=", reload:"&" },
        link: function($scope, el) {
            typeof $scope.nav == 'undefined' ? $scope.nav = { addMode: false} : $scope.nav.addMode = false;

            $scope.pencilClick = function() {
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, FTPL.blog_post_pop, {onTop: true});
            }

            $scope.imgCb = function(action, val) {
                console.info('imgCb blogList', action);
                action == 'save' && $scope.save();
            }

            $scope.save = function() {
                console.log( $scope.mainList , ' main list blog item');
                api.post('blog/saveBlog', $scope.mainList, function(blogResp) {
                    //$scope.pencilClick();
                    $scope.reload();
                });
            }
            $scope.delete = function() {
                notifSvc.warn('Delete this blog and all items?', 5, function(yN) {
                    if (typeof data == 'undefined') {
                        var url = !window.REST ? 'blog/delBlog?uid=' + $scope.mainList.uid :
                                    'blog/delBlog/' + $scope.mainList.uid;
                        yN && api.del(url, function(blogResp) {
                            //$scope.pencilClick();
                            $scope.reload({data: blogResp.data});
                        });
                    }
                });
            }

            $scope.$watch('mainList', function(n,o) {
                typeof n != 'undefined' && n.title == '' && $scope.pencilClick();
            });
       }
    }
}])
.directive('blog', ['FTPL', 'api','loginSvc', '$location', 'dragSvc', 'notifSvc', function(FTPL, api, loginSvc, $location, dragSvc, notifSvc) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.blog_posts,
        scope:false,
        link: function($scope, el) {
            typeof $scope.nav == 'undefined' ? $scope.nav = { addMode: false} : $scope.nav.addMode = false;

            $scope.saveOrder = function() {
                var ord = $scope.blogs.map(function(b) { return b.uid; });

                api.post('blog/reorderBlogs', {uids: ord}, function(resp) {
                    notifSvc.ok('Blogs were reordered', 3);
                    $scope.pencilClick();
                });
            }

            $scope.reorder = function() {
                var reorderTpl = '<div ng-if="reorder" reorder order-list="blogs" handle="title" save-order="saveOrder()"></div>';
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, reorderTpl, {topRight: true});
            }

            $scope.pencilClick = $scope.reorder;

            $scope.reload = function(data) {
                console.info( ' in RELOAD ', data);
                if (typeof data == 'undefined') 
                    api.get('blog/getBlogs', function(blogResp) {
                        $scope.blogs = blogResp.data;
                        $scope.pgrAction('init', function(d) { $scope.blogs = d; }, 'blogs');
                    });
                else 
                    $scope.blogs = data;
            };

            $scope.reload();

            $scope.addItem = function(sub_sub) {
                if ( typeof $scope.blogs[0] == 'undefined')
                    $scope.blogs = [{}];
                else if ( typeof $scope.blogs[0].uid == 'undefined')
                        $scope.blogs.shift();
                    else 
                        $scope.blogs.unshift( {title: '', img: './image/noimage.png', content: '', keywords: '', created: new Date() } );
            }
        }
    }
}])
