var INFO = console.info;
var WARN = console.warn;

window.execs = {};
typeof window.wrGlobal == 'undefined' && (window.wrGlobal = {});
APP
.factory('dragSvc', ['$compile', 'FTPL', '$compile', function($compile, FTPL, $compile) {
    /* DRAGSVC */
    var wrapEl = null;
    var popEl = null;
    var currScope = null;
    var allPOP = angular.element(document.getElementById('allPOP'));
    var overTop = false;
 
    var offY;
    var offX;
    function dragStartFn(ev) {
        var bounds = ev.target.getBoundingClientRect();
        offY = Math.abs(bounds.top - ev.clientY); 
        offX = Math.abs(bounds.left - ev.clientX);

        ev.stopPropagation();
        popEl[0].style.opacity = '0';
    }   
    var popOpts = {};

    function dragFn(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        var handle = null;
        if (ev.clientX > 0 && handle == null) {
            popEl[0].style.opacity = '1';
            handle = setTimeout( function() {
                popEl[0].style.top = (ev.clientY - offY) + 'px'; 
                ev.clientX - offX > 0
                    && (popEl[0].style.left = (ev.clientX - offX) + 'px');
                handle = null;
                localStorage.lastPos = JSON.stringify([ev.clientY, ev.clientX-offX]);
            }, 40);
        }
    }

    function unAnchor(dontToggle ) {
        popEl[0].style.right = 'auto';
        localStorage.anchored = false;
        console.log( ' right auto ');
        return false;
        dontToggle || (currScope.anchored = !currScope.anchored);

        currScope.anchored == true
            ? popEl[0].style.right = '20px'
            : popEl[0].style.right = 'auto';

        localStorage.anchored = currScope.anchored;
        bounds = popEl[0].getBoundingClientRect();
        if (bounds.left - window.innerWidth > -20)
            popEl[0].style.left = window.innerWidth - (bounds.right - bounds.left + 40) + 'px';
        fixWidth();
    }

    function fixWidth() {
        currScope.widthFixed = ! currScope.widthFixed;

        bounds = popEl[0].getBoundingClientRect();
        console.log( bounds.left, bounds.right );
        popEl[0].style.width = currScope.widthFixed ? 'auto' : (bounds.right - bounds.left) + 'px';
    }

    function mkPop(el) {
        var cls = typeof popOpts.cls == 'undefined' ? 'popDefault' : popOpts.cls;

        var popTpl = '<pop-it class="active CLASS_NAME no-ng-class="edtme|checkActive"></pop-it>'
                        .replace('CLASS_NAME', cls);
        popEl = $compile(angular.element(popTpl))(currScope);
        var body = angular.element(document.body);
        if (true) 
            el.append(popEl);
        else 
            body.append(popEl);
    }

    var lastAnchored = true;

    var menuHeight = 55;
    function reg(el) {
        el[0].addEventListener('drag', dragFn); 
        el[0].addEventListener('dragstart', dragStartFn); 

        lsLastPos = localStorage.lastPos;
        typeof lsLastPos == 'undefined' 
            ? lsLastPos = [20, 20]
            : lsLastPos = JSON.parse(localStorage.lastPos);

        popEl[0].style.top  = lsLastPos[0] + 'px'
        popEl[0].style.left = lsLastPos[1] + 'px'

        setTimeout( function() {
            elBounds = el[0].getBoundingClientRect();
            elBounds.height == 0 && (elBounds = el.find('*')[0].getBoundingClientRect());
            bounds   = popEl[0].getBoundingClientRect();

            var shortWin = false;
            var narrWin  = false;

            var topPos  = elBounds.top - bounds.height - 10;
            if (topPos < menuHeight) 
                if (window.innerHeight > elBounds.bottom + bounds.height + 10)
                    topPos  = elBounds.bottom + 10;
                else 
                    shortWin = true;

            var leftPos = elBounds.right + 10;
            if (window.innerWidth <  leftPos + bounds.width)
                if (elBounds.left - bounds.width > 10) 
                    leftPos = elBounds.left - bounds.width - 10;
                else 
                    narrWin = true;

            if (narrWin)
                if (shortWin) {
                    topPos  = window.innerHeight - bounds.height;
                    leftPos = window.innerWidth - bounds.width;
                } else
                    leftPos  =  window.innerWidth - elBounds.left - bounds.width > 0 
                                ? elBounds.left : window.innerWidth - bounds.width - 20;
            else
                if (shortWin)
                    topPos = window.innerHeight - elBounds.top - bounds.height > 0 
                             ? elBounds.top : window.innerHeight - bounds.height;
                else 
                    leftPos  =  window.innerWidth - elBounds.left - bounds.width > 0 
                                ? elBounds.left : window.innerWidth - bounds.width - 20;

            console.log('shortWin', shortWin, 'narrWin', narrWin, 'topPos', topPos, 'leftPos', leftPos);
            slideIn(bounds, topPos, leftPos);
        }, 100);
    }

    var elBounds;

    function slideIn(bounds, topPosEnd, leftPosEnd) {
        var jump = 8;
        var jumpCnt = jump;
        var leftPos = bounds.left;
        var topPos  = bounds.top;

        var shiftX = (bounds.left - leftPosEnd)/jump;
        var shiftY = (bounds.top  - topPosEnd)/jump;

        var hh = setInterval( function() {
            topPos  -= shiftY;
            leftPos -= shiftX;
            angular.element(popEl[0]).css('left', leftPos + 'px');
            angular.element(popEl[0]).css('top',  topPos + 'px');

            (jumpCnt-- <= 1) && clearInterval(hh);
        }, 25);

        localStorage.lastPos = JSON.stringify([topPosEnd, leftPosEnd]);
    }
    
    function init($scope, el, html, opts) {
        destroy();
        if ($scope.edtme) {
            popOpts = typeof opts == 'undefined' ? {} : opts; 
            currScope   = $scope;
            mkPop(el);
            //wrapEl      = el;
            wrapEl      = angular.element(document.body);
            popEl.append($compile(FTPL.cms_popHeader + html)($scope));
            popOpts.anchorRight = typeof popOpts.anchorRight != 'undefined' && popOpts.anchorRight;
            reg(el);
        }
    }

    function destroy() {
        currScope == null || console.log('DESTROY', currScope.$id);
        currScope == null || (currScope.edtme = false);
        //currScope == null || (currScope.nav.edtme = false);

        if (wrapEl != null) {
            wrapEl[0].removeEventListener('drag', function() {});
            wrapEl[0].removeEventListener('dragstart' , function() {});
        }

        if (popEl != null)  while (popEl[0].firstChild)
                                popEl[0].removeChild(popEl[0].firstChild);
        popEl == null || popEl.remove();

        popEl       = null;
        currScope   = null;
        wrapEl      = null;
    }

    function initFn() {
        if (typeof popEl[0] != 'undefined') {
            var bounds = el[0].getBoundingClientRect();
            var elTop  = bounds.top; 
            var elLeft = bounds.left; 

            if (elTop  < 30) elTop = 30;
            if (elLeft < 30) elLeft = 30;
            popEl[0].style.left = elLeft + 'px'; 

            setTimeout( function() {
                if (!popOpts.onTop)
                    if ( elTop - popEl[0].clientHeight > 20)
                        elTop = elTop-popEl[0].clientHeight - 20;
                    else 
                        elTop = bounds.bottom + 20;
                else 
                    elTop = bounds.top + 100;

                popEl[0].style.top = elTop + 'px';

                var overflow = window.innerWidth - (bounds.left + popEl[0].clientWidth);
                if (overflow < 0) {
                    elLeft += overflow - 30;
                    elLeft < 30 && (elLeft = 30);
                    popEl[0].style.left = elLeft + 'px'; 
                 }
            }, 50);
        }
    }


    return {
        //reg: reg,
        init: init,
        destroy: destroy 
    }
}])
.factory('swSvc', ['$http', 'deviceDetector', function($http, deviceDetector) {

    if (false && deviceDetector.browser == 'chrome') {
        window.GoogleSamples = window.GoogleSamples || {};
        window.GoogleSamples.Config = window.GoogleSamples.Config || {
            gcmAPIKey: 'AIzaSyDRpp6ne8leBimATjx7RtLGFBmhwcrop8k' // serverGoPush
            //gcmAPIKey: 'AIzaSyAvQVg0_ajhS9lGHatoQoPVSHwPnSoNhXs' // devServer
        };
    }

    var ret = {
        updCreds: function(cb) {
            ret.msg({
                command: 'passCreds',
                msg     : window.wrGlobal.user_uid,
                sessId  : window.wrGlobal.sessId,
                login   : window.wrGlobal.name,
                passwd  : window.wrGlobal.passwd,
                pollInt : 2000
             }, cb);
        },
        msg: function(msgObj, cb) {
            var messageChannel = new MessageChannel();
            messageChannel.port1.onmessage = cb; 

            setTimeout( function() {
                navigator.serviceWorker.controller.postMessage(msgObj, [messageChannel.port2]);
            }, 0); // TODO: for now, later tie it to ready
        },
        setupNotifs: function(cb) {
            Notification.requestPermission(function(result) {
                if (result === 'granted') 
                    navigator.serviceWorker.ready.then(function(r) { 
                        cb(r);
                        r.pushManager.subscribe({userVisibleOnly: true}).then( 
                            function(sub) {
                                $http.get('infra/pushId?id=' + sub.endpoint).then(function(r) {
                                    //console.log( 'PushIds ' + (r.data.success ? '' : 'NOT ') + 'Updated at Server');
                                });
                            }
                        );
                    });
            });
        },
        setupSafari:function(cb) {
            if ('Notification' in window) { 
                notify = function(title, body) {
                    console.log( 'NOTIFYING SAFARI');
                    if (Notification.permission === 'default') {
                        Notification.requestPermission(function () { notify(title, body); });
                    }
                    else if (Notification.permission === 'granted') {
                        var n = new Notification( title, {body: body, tag: 'unique string'} );
                        n.onclick = function () { this.close(); };
                        n.onclose = function () { console.log('Notification closed'); };
                    }
                    else if (Notification.permission === 'denied') { return; }
                };
                cb(notify);
            }
        },
        clearSWChrome:function() {
            navigator.serviceWorker.getRegistrations().then(function(registrations) {
                if (registrations.length > 0)
                    for (var i=0; i<registrations.length; i++)
                        registrations[i].unregister();
            });
        },
        setupChrome:function(swCb, noSwCb) {
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker.register('./sw.js').then(function(reg) {
                    reg.id = 1;
                    swCb(reg); 
                });
            } else {
                noSwCb();
            }
        }
    }

    return ret;
}])
.factory('idbSvc', [function() {
    var ver = 4;
    function del(cb) {
    /*
        var dbReq = window.indexedDB.deleteDatabase("credsDb");
        dbReq.onerror = function() {
            console.log( 'ERROR WHEN deleted db...........', dbReq);
            cb(false);
        };

        dbReq.onsuccess = function() {
            console.log( 'deleted db...........');
            cb(true);
        };

        console.log( ' IN DELETE DB ');
        */
        cb(false);
    }

    function put(id, val) {
        var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

        // Open (or create) the database
        var open = indexedDB.open("credsDb", ver);

        // Create the schema
        open.onupgradeneeded = function(event) {
            var db = open.result;
            var store = db.createObjectStore("swconn", {keyPath: "id"});
            var index = store.createIndex("NameIndex", ["creds.login"]);
        };

        open.onsuccess = function() {
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction("swconn", "readwrite");
            var store = tx.objectStore("swconn");
            var index = store.index("NameIndex");

            // Add some data
            store.put({id: id, creds: val});

            tx.oncomplete = function() { db.close(); };
        }
        open.onerror = function(err) {
            console.log( 'PUT OPEN ERROR IDB', err, open);
        }
    }

    function get(id) {
        var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

        var open = indexedDB.open("credsDb", ver);

        return new Promise(function(y, n) {
            open.onsuccess = function() {
                var db      = open.result;
                var tx      = db.transaction("swconn", "readwrite");
                var store   = tx.objectStore("swconn");
                var getVal  = store.get(id);

                getVal.onsuccess = function()    { y(getVal.result.creds); };
                getVal.onerror   = function(err) { n(err); };
                tx.oncomplete    = function()    { db.close(); };
            }
            open.onerror= function(err) {
                console.log( err,' err in idbsvc get', open);
                n(err);
            }
        });
    }

    return { get:get, put:put, del:del };
}])
.factory('api', ['$http', function($http) {
    var urlHref = document.location.origin + '/';

    function get(url, qString, cb) {
        if (['blo', 'sto', 'fil', 'cms'].indexOf(url.substring(0,3)) > -1) url = 'api/' + url;
        if (typeof qString == 'function') {
            cb = qString;
            qString = '';
        }

        typeof cb == 'function' || (cb = function() {});
        qString == '' || (qString = '?' + qString);

        $http.get(urlHref + url + qString).success(cb).error(cb);
    }

    function post(url, data, token, cb, method) {
        method = typeof method == 'undefined' ? 'POST' : method;
        if (typeof cb == 'undefined') {
            cb = token;
            token = '';
        }
        if (['blo', 'sto', 'fil', 'cms'].indexOf(url.substring(0,3)) > -1) url = 'api/' + url;

        typeof cb == 'function' || (cb = function() {});
        $http({
            method: method,
            url:    urlHref + url,
            data:   data,
            headers: {'Content-Type': 'application/json', 'Authorization': token}
        }).success(function(resp) {
            cb(resp);
        }).error(function(err) {
            cb([err, 'err']);
        });
    }

    function del(url, data, token, cb) {
        post(url, data, token, cb, 'DELETE');
    }

    return {
        get:    get,
        //put:    put,
        del:    del,
        post:   post
    }
}])
.factory('loginSvc', ['$http', 'config', '$location', 'LS', function($http, config, $location, LS) {
    var execs = [];
    var loggedIn = false;
    var devs = [    '1ffb11b5-dc0c-b207-1f82-827d5295848b', //mathieu
                    '382a16f8-a05b-11e6-9920-fa163edcdeb9', //tuc
                    '3ad45fe4-a05b-11e6-9920-fa163edcdeb9'  //ruby
               ];

    var ret = {
        LS: LS,
        isDev: function() {
            return ret.LS('perms', null, 'jkauth').split(':').indexOf('DEV') > -1;
        },
        isMon: function() {
            return ret.LS('perms', null, 'jkauth').split(':').indexOf('MON') > -1;
        },
        getLocalStorage : function($scope) {
            var jkAuth = localStorage.jkauth;
            if (typeof jkAuth != 'undefined') {
                $scope.userName     = JSON.parse(jkAuth).userName;
                $scope.loginChecked = JSON.parse(jkAuth).loginChecked;

                if (JSON.parse(jkAuth).loginChecked && JSON.parse(jkAuth).passwd !='') {
                    $scope.passwd       = JSON.parse(jkAuth).passwd;
                    ret.login($scope, '', false);
                } else {
                    $scope.loginScreen = true;
                }
            } else {
                $scope.loginScreen = true;
            }
        },
        login : function($scope, cb) {
            $scope.useGoogle = false;
            var lsCreds  = LS('auth.creds');
            console.info( lsCreds,' ls creds ');

            function goodLogin(data) {
                loggedIn = data.success;
                if ( loggedIn ) {
                    $scope.auth.loggedIn = true;
                    $scope.initMenu = true;

                    try {
                        var authA           = data.msg.split(":");
                    } catch (e) {
                        var authA           = data.creds.split(":");
                    }

                    $scope.auth = data;

                    $scope.$broadcast('loggedIn', data);

                    ret.execExecs();
                } else {
                    console.error('Login Failed:' , data);
                }
                typeof cb == 'function' && cb(data);
            }

            var mobLoginQuery   = '&mob=' + $scope.isMobile;

            if (typeof lsCreds == 'undefined' || lsCreds == '' || $scope.auth.passwd) {
                console.log( $location.host(), $location.host().substring(0, $location.host().indexOf('.')),  ' location href...>>>><<<<');
                if ($location.host().substring(0, $location.host().indexOf('.')) == 'xyz')
                    var loginUrl = 'api/auth/apiLogin/' + $scope.auth.login + '/' + $scope.auth.passwd;
                else
                    var loginUrl = 'auth/login/?userName=' + $scope.auth.login + '&passwd=' + $scope.auth.passwd;
            } else {
                if (lsCreds.passwd == '') 
                    lsCreds.passwd = $scope.passwd;
                
                (typeof lsCreds.passwd == 'undefined' || !lsCreds )
                    && (lsCreds.passwd = $scope.auth.passwd);
                    
                var loginUrl = 'auth/login/?userName=' + lsCreds.login + '&passwd=' + lsCreds.passwd;
            }
            

            //loginUrl = 'auth/login/' + $scope.auth.login + '/' + $scope.auth.passwd;
            $http.get(loginUrl).success(function(data) {
            //$http.get(loginUrl).success(function(data) {
                if ($scope.loggedIn = data.success) {
                    data.loginChecked = $scope.auth.loginChecked;
                    data.passwd = data.loginChecked ? $scope.auth.passwd : null;

                    LS('auth.creds', data);
                    console.log( 'ls GOOD creds',LS('auth.creds'));
                    goodLogin(data);
                    $scope.loginScreen = false;
                    $scope.failedLogin = false;
                }
                else {
                    $scope.failedLogin = true;
                    $scope.loginScreen = true;
                    LS('auth.creds','');
                    console.log(  'Not logged in, maybe GOOGLE login next ', data);
                    typeof cb == 'function' && cb(data);
                }
            });
        },
        logout: function($scope, cb) {
            console.log( 'XXXXXXXXXXXXXXXXXXXXXXXX');
            if ($location.host().substring(0, $location.host().indexOf('.')) == 'xyz')
                var url = 'api/auth/apiLogout/noop/logout';
            else 
                var url = 'auth/logout?mob=' + $scope.isMobile;

            $http.get(url).success(function(data) {
                if (data.success) {
                    $scope.loggedIn = false;
                    $scope.initMenu = false;
                    $scope.loginScreen = true;
                    $scope.passwd = '';

                    var lsCreds = LS('auth.creds');
                    lsCreds.passwd = '';
                    LS('auth.creds', lsCreds);

                    document.location='/#/login';
                }
                typeof cb == 'function' && cb(data);
            });
        },
        checkClicked: function($scope) {
            console.log($scope.loginChecked);
            if ($scope.checkClicked) {
            }
        },
        runAfterLogin: function( fn ) {
            loggedIn ? fn() : execs.push(fn);
        },
        execExecs: function() {
            execs.map(function(fn) { fn(); });
        }
    }

    return ret;
}])
.factory('dataSvc', ['api',function(api) {

    var urls = {
        users:          'infra/getUsers',
        clients:        'infra/getClients',
        files:          'files/getDirs',
        sharedFiles:    'files/getSharedDirs',
        notifsForMe:    'activities/getNotifsForMe',
        notifsFromMe:   'activities/getNotifsFromMe'
    };
    var promises = {};

    var me = {
        set:        function(set, inData)   { data[set].d = inData;   },
        promise:    function(set, cb)       { 
            typeof promises[set] == 'undefined' && (promises[set] = []);
            promises[set].push(cb); 
        },
        get:    function(set, q, cb, noRefresh) {
            console.log( 'getting ' + set, noRefresh);
            noRefresh = typeof withRefresh == 'undefined' || !noRefresh;

            if ( me[set] && noRefresh) {
                cb(me[set]);
            } else {
                me.promise(set, cb);
                if (typeof me[set] == 'undefined') {
                    me[set] = false; // first load in progress
                    me.refresh(set, q);
                }
            }
        },
        refresh:    function(set, q, cb) {
            if (typeof promises[set] != 'undefined') {
                q || (q = '');
                api.get(urls[set], q, function(resp) {
                    me[set] = resp;
                    setTimeout( function() {
                        promises[set].map(function(cb) { cb(me[set]); })
                        promises[set] = [];
                        typeof cb == 'function' && cb();
                    }, 0);
                });
            } else typeof cb == 'function' && cb(false);
        },
        getFromDb: function(set, q, cb) {
            api.get(urls[set], q, function(resp) {
                typeof cb == 'function' && cb(resp);
            });
        }
    }
    return me;
}])
//.factory('listSrv', ['$http', function($http) { }])
.factory('ready', ['dataSvc', function(dataSvc) {
    var execs = window.execs; //{};
    var perms = null;

    var me = {
        register: function(handle, cb, delay) { 
            execs[handle] = [cb, delay];
        },
        runOne: function(handle) {
            execs[handle][0]();
        },
        run: function(auth) {
            perms = auth;
            var handles = [];
            for (var handle in execs) {
                handles.push(handle);
                if ( typeof execs[handle][1] == 'undefined' && parseInt(execs[handle][1]>0) ){
                    setTimeout(execs[handle][0], execs[handle][1]);
                } else {
                    execs[handle][0]();
                }
            }
        },
        get: function(url, q, cb, withRefresh)  { 
            dataSvc.getFromDb(url, q, cb, withRefresh); 
        },
        refresh: function(url, q, cb)  { 
            dataSvc.refresh(url, q, cb); 
        },
        getPerms: function(cb) { 
            perms ? cb(perms) : me.register(function() { cb(perms); });
        }
    }
    return me;
}])
.factory('config', [function() {
    var config = {};

    return {
        set: function(k, v) { config[k] = v; },
        get: function(k)    { return config[k] },
        reg: function(cb)   { typeof cb == 'function' && cb(); }
    }
}])
.factory('LS', [function() {
    return function(key, val, inLsKey) {
        var lsKey = typeof inLsKey == 'undefined' ? 'MYSTORE' : inLsKey;
        var ls = localStorage[lsKey];

        if (typeof key == 'undefined' && typeof val == 'undefined') {
            return localStorage;
        } else if (val == 'CLEAR') {
            localStorage.removeItem(key);
        } else if (val == null && key == null) {
            return JSON.parse(localStorage[inLsKey]);
        } else {
            if (typeof ls == 'undefined') {
                localStorage[lsKey] = JSON.stringify({}); 
                ls = localStorage[lsKey];
            }

            ls = JSON.parse(ls);

            typeof key == 'string' && ( key = key.split('.') );
            var item = angular.copy(ls);

            for (var i=0; i<key.length - 1; i++) {
                item = ls[key[i]]; 
                if (typeof item == 'undefined') {
                    item = ls[key[i]] = {};
                } else {
                    item = ls[key[i]];
                }
            }
            typeof item == 'undefined' && (item = ls[key[key.length-1]]);

            if (typeof val === 'undefined' || val === null) {
                ls = item[key[key.length-1]];
            } else {
                item[key[key.length-1]] = val;
                localStorage[lsKey] = JSON.stringify(ls); 
            }
            return ls;
        }
    }
}])
.directive('notifier', ['FTPL', 'notifSvc', 'api', function(FTPL, notifSvc, api) {
    return {
        restrict: 'A',
        replace: true,
        scope: false,
        template: FTPL.notifier,
        link: function($scope, el) {
            var timeoutHandle = false;
            notifSvc.activate($scope);
            confirmCb = null;

            $scope.confirmResp = function(isYes) {
                $scope.notifActive = false;
                if (typeof confirmCb == 'function') 
                    confirmCb(isYes);
                else
                    isYes && $scope.notifYes();
            }

            $scope.notifYes = function() {
                api.get('infra/delPatient?uid=' + $scope.delUid, function(resp) {
                    console.log(resp, 'delPatient executed');
                    if (resp.success) {
                        notifSvc.notify('Patient deleted.', 5000, 'success');
                    }
                    else {
                        notifSvc.notify('Something went wrong.', 5000, 'error');
                    }
                });
            }

            $scope.notify = function(msg, dur, type, confCb) {
                
                clearTimeout(timeoutHandle);
                $scope.confirmOption = false;

                timeoutHandle = setTimeout(function() {
                    $scope.notifActive = false;
                    $scope.$$phase || $scope.$digest();
                }, dur);

                $scope.msg = msg;
                if (typeof confCb != 'undefined' && confCb ) { $scope.confirmOption = true; }

                //INFO( confCb , msg, dur, 'confcb', $scope.confirmOption);

                var ret;
                switch (type) {
                    case 'success' : ret = 'check';                 break;
                    case 'info'    : ret = 'info';                  break;
                    case 'error'   : ret = 'ban';                   break;
                    case 'warning' : ret = 'exclamation-circle';    break;
                }

                $scope.iconType = ret;
                $scope.notifActive = true;

                typeof confCb == 'function' && (confirmCb = confCb);
            }
        }
    }
}])
.factory('notifSvc', [function() {
    var scope = {};
    var timeoutHandle = false;

    function notifFn(severity, key, dur, conf) {
        typeof dur == 'undefined' && (dur = 3);
        //var msg = typeof trans.lang[key] == 'undefined' ? key : trans.lang[key];
        var msg = key;
        scope.notify(msg, dur*1000, severity, conf);
    }

    var me = {
        activate: function($scope) {
            scope = $scope;
        },
        notify: function(msg,dur, type, conf) { scope.notify(msg, dur, type, conf); },

        ok:     function(key, dur, cb) { notifFn('success', key, dur, cb); },
        info:   function(key, dur, cb) { notifFn('info',    key, dur, cb); },
        err:    function(key, dur, cb) { notifFn('error',   key, dur, cb); },
        warn:   function(key, dur, cb) { notifFn('warning', key, dur, cb); },

        close: function() {
            clearTimeout(timeoutHandle);
            scope.notifActive = false;
        },
        sendUid: function(uid) {
            scope.delUid = uid;
            console.log(scope.delUid, ' uid from sendUid');
        }
    }

    return me;
}])
.factory('DT', [function() {
    function pad(i) {
        return i<10 ? '0' + i : i;
    }
    function getZ(dt) {
        var z = {};
        z.yr = dt.getFullYear();
        z.mo = pad(dt.getMonth() + 1);
        z.day = pad(dt.getDate());
        z.hr = pad(dt.getHours());
        z.mi = pad(dt.getMinutes());
        z.sec = pad(dt.getSeconds());
        return z;
    }

    var me = {
        later:function(m, h, d, dt) {
            var dt = typeof dt == 'undefined' ? new Date() : new Date(dt);
            if (typeof m != 'undefined' && m) dt.setMinutes(dt.getMinutes() + parseInt(m));
            if (typeof h != 'undefined' && h) dt.setHours(dt.getHours() + parseInt(h));
            if (typeof d != 'undefined' && d) dt.setDate(dt.getDate() + parseInt(d));

            var z = getZ(dt);
            var retDt = z.yr + '-' + z.mo + '-' + z.day + ' ' + z.hr + ':' + z.mi + ':' + z.sec;
            return retDt;
        },
        format:function(dt) {
            var dt = typeof dt == 'undefined' ? new Date() : new Date(dt);

            var z = getZ(dt);
            var retDt = z.yr + '-' + z.mo + '-' + z.day + ' ' + z.hr + ':' + z.mi + ':' + z.sec;
            return retDt;
        }
    }
    return me;
}])
;
