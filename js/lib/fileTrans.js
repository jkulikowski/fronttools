APP
.factory('fileTrans', ['Upload', '$http', function(Upload, $http) {
    var urlBase = window.URLHREF + '/DataFiles/';
    var pom = document.createElement('a');
    var anchor = document.createElement('a');
    var img = document.createElement('img');
    //anchor.appendChild(document.createElement('img'));

    function aClick(aEl) {
        if (document.createEvent) {
            var event = document.createEvent('MouseEvents');
            event.initEvent('click', true, true);
            aEl.dispatchEvent(event);
        }
        else aEl.click();
    }

    function upload(url, params, cb, progCb) {
        console.info( 'upload params', params );
        Upload.http({ url: url, data: params})
            .then(function (resp) {
                console.log( resp, ' resp ' );
                console.log('Success ' + resp.config.data.fileName + 'uploaded. Response: ');
                cb(resp, resp.config.data.fileName);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                progCb(evt.config.data.fileName , progressPercentage);
            });
        ;
    }

    return {
        downloadXML: function(filename, content) {
            pom.setAttribute('href', 'data:content/xml;charset=utf-8,' + encodeURIComponent(content));
            pom.setAttribute('download', filename);
            aClick(pom);
        },
        downloadUrl: function(url, type, viewOnly) {
            anchor.setAttribute('href', url);
            //angular.element(anchor).find('img')[0].setAttribute('src' , url);
            viewOnly    ? anchor.setAttribute('target', '_blank')
                        : anchor.setAttribute('download', 'df.png');
            aClick(anchor);
        },
        upload: function(params, isNode) {
            var url = typeof isNode == 'undefined'  ? 'DataFiles/uploadFile'
                                                    : 'http://ns396794.ip-178-33-230.eu/fs';

            Upload.upload({ url: url, data: params})
                .then(function (resp) {
                    console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
                    console.log(resp);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                });
        },
        linker: function fileUpLinker($scope, el, attrs) {
                var files = [];
                $scope.files = [];
                $scope.change = function(isDrop) {
                    setTimeout( function() {
                        for (var i=0; i<$scope.files.length; i++) {
                            $scope.fileList.push($scope.files[i].name);
                            files.push($scope.files[i]);
                        }
                        $scope.$digest();
                    }, 200);
                }

                var dropMsg = 'Drop files here or click to upload';
                //var dropMsg = 'Déposer fichiers ou cliquer pour télécharger';
                $scope.clear = function() {
                    $scope.uploading = false;
                    $scope.fileList = [];
                    $scope.files = [];
                    $scope.errMsg = '';
                    $scope.msgList = {};
                    $scope.upMsg = dropMsg; 
                    $scope.$$phase && $scope.$digest();
                    $scope.done = false;
                    existingFiles = [];
                }

                $scope.clear();

                var existingFiles = [];
                var fileToUpl = [];
                var uploadedCount = 0;

                function uploadOne(params, rdrLength) {
                    $scope.uploading = true;
                    
                    upload('files/uploadFile', params, function(data, fileName) {
                        data = data.data;
                        console.info( 'DATa' , data );
                        if (data.success == true) {
                            //$scope.files = '';
                        } else {
                            existingFiles.push(params.fileName);
                            $scope.errMsg = 'Error: File name ' + existingFiles + ' already exists.';
                        }
                        uploadedCount++;
                        $scope.openFiles();
                        if (rdrLength == uploadedCount) {
                            $scope.upMsg = dropMsg;
                         }
                    }, function(fileName, percent) {
                        $scope.msgList[fileName] = percent;
                        console.log($scope.msgList, ' msgList');

                        for (var i in $scope.msgList) {
                            if ($scope.msgList[i] != 100) {
                                $scope.done = false;
                                break;
                            }
                            else { 
                                $scope.done = true;
                                $scope.upDone({fileName:fileName});
                            }
                            console.log($scope.done, ' done');
                        }
                    });
                }

                $scope.submit = function() {
                    $scope.upMsg = 'Preparing files for upload ...';
                    $scope.fileList = [];
                    $scope.msgList  = {};
                    uploadedCount   = 0;
                    var rdr         = [];

                    for (var i=0; i<$scope.files.length; i++) {
                        var ext = $scope.files[i].name.trim().split('.').pop().toUpperCase();
                        rdr[i] = new FileReader();
                        var loaded = 0;
                        console.log( 'ZZZ', $scope.grp);
                        $scope.grp = typeof $scope.grp == 'undefined' ? 'main' : $scope.grp;
                        rdr[i].onload = function(){
                            ++loaded==$scope.files.length && rdr.map(function(r, i) {
                                uploadOne({
                                    file:       ['DAT'].indexOf(r.ext) != -1
                                                    ? btoa(r.result) : r.result,
                                    dirName:    $scope.grp,
                                    dirUid:     $scope.dirUid,
                                    userUid:    window.wrGlobal.user_uid,
                                    fileName:   $scope.files[i].name,
                                    fileType:   r.result.substring(5,9),
                                    ext:        ext
                                }, rdr.length);
                            });
                        }

                        var sizeInMegs = (parseInt($scope.files[i].size/10000)*10)/1000;

                        ['DAT'].indexOf(ext) != -1
                            ? rdr[i].readAsBinaryString($scope.files[i]) 
                            : rdr[i].readAsDataURL($scope.files[i]);
                    }
                }

                $scope.delDir = function(){
                    $http.get('DataFiles/deleteDir?guid=we_dont_have_it').success(function( ret ) {
                        console.log( ret , ' from delete dir ' );
                    });
                };
            }
    }
}])
.directive('upFile', ['fileTrans', 'FTPL', function(fileTrans, FTPL) {
    return {
        restrict: 'AC',
        scope:{ openFiles: "&", newFile: "=", upMsg: "=", errMsg: "=" , upDone: "&", grp: "="},
        replace: false, 
        template: FTPL.lib_upFile,
        link:  fileTrans.linker
    }
}])
;
