typeof google.visualization == 'undefined' &&
    google.load('visualization', '1.0', { callback : function() {}, packages: ['corechart', 'table'] });

APP 
.factory('chartSvc', [function() {
    var baseColors = ["#3366cc","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00",
                      "#b82e2e","#316395","#994499","#22aa99","#aaaa11","#6633cc","#e67300","#8b0707",
                      "#651067","#329262","#5574a6","#3b3eac","#b77322","#16d620","#b91383","#f4359e",
                      "#9c5935","#a9c413","#2a778d","#668d1c","#bea413","#0c5922","#743411"];
    var colors = angular.copy(baseColors);
    return {
        chartFn: function(typeAttr, el) {
            var chartName = 'BarChart';
            switch (typeAttr) {
                case 'line' : chartName = 'LineChart'; break;
                case 'step' : chartName = 'SteppedAreaChart'; break;
                case 'hist' : chartName = 'Histogram'; break;
                case 'area' : chartName = 'AreaChart'; break;
                case 'pie'  : chartName = 'PieChart'; break;
                case 'scat' : chartName = 'ScatterChart'; break;
                case 'column' : chartName = 'ColumnChart'; break;
                case 'table' : chartName = 'Table'; break;
                default:      chartName = 'BarChart';  break;
            }
            return (new google.visualization[chartName](el.find('chart')[0]));
        },
        chartArea: function(typeAttr) {
            var area =  {width: '70%', height: '70%'};
            switch (typeAttr) {
                case 'area' :
                case 'scat' :
                case 'line' : area = {width: '86%', height: '60%', left:'50', right:'30'}; break;
                case 'column': area = {width: '92%', height: '60%', left:'40', right:'10', bottom:'40'}; break;
                case 'step' : area = {width: '92%', height: '60%', right:'0', left:'40', top:'65', bottom:'10'}; break;
                case 'pie'  : area = {width: '80%', height: '94%', right: '0', top:'20'}; break;
                case 'bar-nar' : area = {width: '80%', height: '70%', right: '0', left:'90', bottom:"0"}; break;
                case 'bar-med' : area = {width: '60%', height: '60%'}; break;
                case 'bar' : area = {width: '80%', height: '60%', left:'120', right:'0'}; break;
                default:      break;
            }
            area.width = '100%';
            return area;
        }, 
        colors: function(rot) {
            if (false)
            if (typeof rot != 'undefined') {
                colors = [];
                for (var i=rot; i<baseColors.length; i++) 
                    colors.push(baseColors[i]);
            }
            return colors;
        },
        dataDiff: function(oldD, newD) {
        /*
            console.log( ' old d ');
            console.log(JSON.stringify(oldD));
            console.log( ' new d ');
            console.log(JSON.stringify(newD));

            var changed = false;
            if ( oldD != null)
                for (var i=0; i<newD.length; i++) {
                    if (changed) break;
                    for (var j=0; j<newD[i].length; j++) {
                        console.log( newD[i][j], oldD[i][j] );
                        if (typeof oldD[i][j] == 'undefined' || typeof newD[i][j] == 'undefined' || newD[i][j] != oldD[i][j]) { 
                            changed = true; break; 
                        }
                    }
                }
            else 
                changed = true;

            console.log( changed , ' changed ' , oldD, newD);
            //return changed;
            */
            return JSON.stringify(oldD) != JSON.stringify(newD);
        }
    }
}])
.directive('chart', ['chartSvc', '$http', 'TPL', function(chartSvc, $http, TPL) { return {
    //templateUrl: 'html/wrChart.html',
    template: TPL.wrChart,
    restrict: 'E', 
    scope: {opts: "=", datain: "=", databack: "&", qstring:"=", quickTitle: "="},
    link: function($scope, el, attrs) {
        var qString = '';
        var autoPlay = typeof attrs.autoPlay != 'undefined';
        $scope.selOpts = ['pie', 'bar', 'line', 'step', 'hist', 'area', 'scat', 'column', 'table'];
        typeof attrs.selOpts == 'undefined' || ($scope.selOpts = JSON.parse(attrs.selOpts));

        angular.extend($scope, {
            playClass  : autoPlay ? 'fa-stop' : 'fa-play',
            play       : autoPlay,
            loading    : false,
            apiUrl     : attrs.apiUrl,
            chartTitle : attrs.title,
            loop       : typeof attrs.initLoop == 'undefined' ? 5 : attrs.initLoop,
            noMins     : typeof attrs.noMins != 'undefined',
            noTime     : typeof attrs.noTime != 'undefined' || !$scope.noMins,
            noComp     : typeof attrs.noComp != 'undefined',
            noUniq     : typeof attrs.noUniq != 'undefined',
            noType     : typeof attrs.noType != 'undefined',
            minsBack   : typeof attrs.initMinBack == 'undefined' ? 60 : attrs.initMinBack,
            cType      : typeof attrs.cType == 'undefined' ? 'bar' : attrs.cType,
            isComp     : typeof attrs.isComp != 'undefined',
            isUniq     : typeof attrs.isUniq != 'undefined',
            noRefresh  : typeof attrs.noRefresh != 'undefined',
        });

        var textColor = '#111';
        var opts = {    width:  900,
                        height: 400,
                        animation: {duration:1000, easing:'out'},
                        isStacked: typeof attrs.noStack == 'undefined',
                        colors:chartSvc.colors(17),
                        //colors:colors,
                        legend: {position: 'top', maxLines: 3, textStyle: {color: textColor}},
                        //legend: { position: 'none' },
                        // titleTextStyle: {color: textColor},
                        chartArea: chartSvc.chartArea($scope.cType),
                        hAxis:{ textStyle: {color: textColor}},
                        vAxis:{ textStyle: {color: textColor}},
                        backgroundColor: 'transparent',
                        pieHole: 0.4,
                        pieSliceText: 'none',
                        //is3D: true,
                        pointSize : 3,
                        fontSize: 12,
                        series  : {0: {pointShape: 'polygon'}, 1: {pointShape: 'diamond'}},
                        smoothLine: false,
                        titlePosition: 'none' };
         if ($scope.cType == 'pie') opts.legend.position = 'right';
        
        // OPTIONS: static override ( from attrs )
        for (var i in opts) typeof attrs[i] == 'undefined' || (opts[i] = attrs[i]);

        $scope.$watch('datain', function(n,o) { 
            n && render();
        });

        var chart       = null;
        var prevData    = null;
        function display(data, forceChange) {
            if (data.length == 1 ) 
                 data = [ ['', 'No Entries',], ['dummy', 0] ];

            //if (data.length > 1) { 
                chart == null && (chart = chartSvc.chartFn($scope.cType, el));

                (forceChange || chartSvc.dataDiff(prevData, data)) && chart.draw(
                    google.visualization.arrayToDataTable( data ), opts
                );

                prevData = angular.copy(data);
                //$scope.loading = true;
                //setTimeout(function() { $scope.loading = false; $scope.$digest(); }, 1400);
            //}
        };

        function render(forceChange) {
            if (    typeof google.visualization == 'undefined' ||
                    typeof google.visualization.BarChart == 'undefined') return false;
            else 
                //if (typeof attrs.datain == 'undefined' ) {
                if (typeof $scope.datain == 'undefined' ) {
                    var urlType = $scope.isComp ? 'Comp' : ($scope.isUniq ? 'Uniq' : '');
                    $http.get('' + $scope.apiUrl + urlType + qString).success(function(data) {
                        if (typeof data.data != 'undefined') {
                            setTimeout( function() {
                                display( data.data, forceChange );
                            }, 0);
                            console.info( typeof $scope.databack );
                            typeof $scope.databack == 'function' && $scope.databack({data: data.data});
                            $scope.databack({data: data.data});
                        } else {
                            //$scope.$emit('noLogin', data.msg);
                            //console.log(' emitted ');
                        }
                    });
                } else {
                    typeof $scope.datain == 'undefined' || display($scope.datain, forceChange);
                }

            return true;
        }

        function bldQString() {
            qString = typeof attrs.query == 'undefined' ? '' : attrs.query;
            typeof $scope.qstring == 'undefined' || (qString += $scope.qstring);

            if (typeof attrs.noMins == 'undefined' && $scope.minsBack != 'inf')
                qString += (qString.length ? '&' : '?') + 'minsBack=' + $scope.minsBack;
        }

        $scope.chartChange = function() {
            chart = null;
            //$scope.playStart(false);
            render(true);
        };

        $scope.playStart = function(toggle) {
            $scope.noUniq = typeof attrs.noUniq != 'undefined' || $scope.isComp;
            $scope.noComp = typeof attrs.noComp != 'undefined' || $scope.isUniq;
            clearInterval(interHandle);

            toggle && ($scope.playClass = ($scope.play = !$scope.play) ? 'fa-stop' : 'fa-play');
            // OPTIONS: dynamic override ( from $scope.opts )
            for (var i in $scope.opts) opts[i] = $scope.opts[i];
            //bldQString();

            $scope.play && render();
            if ($scope.loop > 0 && $scope.play)
                interHandle = setInterval(render, $scope.loop*1000);
        };

        // Initial chart starts here
        bldQString();
        var interHandle = setInterval(function() { render() && $scope.playStart(false); }, 20);
    }}
}])
;
