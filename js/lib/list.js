APP
.directive('list', ['$compile', 'ready', 'api', 'TPL',function($compile, ready, api, TPL) {
    return {
        restrict: 'AC',
        replace: false,
        scope: { selectedId : "=", parentUid : "=" } ,
        //templateUrl: 'html/list/list.html',
        template: TPL.list_list,
        link: function($scope, el, attrs) {

            //console.warn('remove this console entry in list.js, this is TPL, we should start using mtpl again, it is all set up');
            //console.warn('ALSO: HOW MANY list directives are we using?');
            //console.info( TPL );

            $scope.msg = '';
            $scope.hidden = [];
            var shadowData = [];

            var dbCtrl      = typeof attrs.dbCtrl != 'undefined' ? attrs.dbCtrl: 'error';
            $scope.listType = attrs.listType;
            $scope.noAdd    = typeof attrs.noAdd != 'undefined';
            $scope.keyName  = typeof attrs.keyName  != 'undefined' ? attrs.keyName : 'name';
            $scope.type     = typeof attrs.type == 'undefined' ? false : attrs.type;

            var qString = '';
            var columns = null;
            $scope.loading = '';

            var searchIdx = -1;
            var searchVal = '';
            var allData = [];
            var filteredData = [];
            $scope.currPage = 1;
            $scope.perPage = typeof attrs.autoShow == 'undefined' ? 0 : 10;
            var start = 0;
            var end   = $scope.perPage;

            $scope.$watch('selectedId', function(n, o) {
                if (typeof n != 'undefined') {
                    qString = typeof attrs.selectedName == 'undefined'
                        ? ''
                        : '?' + attrs.selectedName + '=' + $scope.selectedId;
                    loadData(qString);
                }
            });

            $scope.runSearch = function(inSearch){
                typeof inSearch == 'undefined' || (searchVal = inSearch);
                if (searchVal && searchIdx> -1 && searchVal.toString().length>0 && allData.length > 0) {
                    var dbName = $scope.listHeaders[searchIdx].dbName;
                    filteredData = [];
                    allData.map(function(i) {
                        var listVal = i[dbName] == null ? '' : i[dbName].toString();
                        try {
                            searchVal.substring(0,1) == '/'
                                ? listVal.match(new RegExp(searchVal.substr(1)))                         && filteredData.push(i)
                                : listVal.toLowerCase().indexOf(searchVal.toString().toLowerCase()) > -1 && filteredData.push(i);
                        } catch (e)  {}
                    });
                } else {
                    filteredData = angular.copy(allData);
                }

                $scope.setPage();
            }
            $scope.sortBy = function(idx, sortDir) {
                var colName = $scope.listHeaders[idx].dbName;
                filteredData.sort(function(a, b) { 
                    if (a[colName] > b[colName]) return sortDir ? 1 : -1;
                    if (a[colName] < b[colName]) return sortDir ? -1 : 1;
                    else return 0;
                });
                shadowData = filteredData.slice(start, start + $scope.perPage);
                $scope.listData = angular.copy(shadowData);
            }

            $scope.setSearch = function(idx, isOn){
                searchIdx = idx;
                $scope.runSearch('');
            }

            $scope.pgList = [1, 2];

            function setStart(upOrDown) {
                var inStart = start;
                typeof upOrDown == 'undefined' && (upOrDown = 'zero');
                switch (upOrDown) {
                    case 'zero' :   start =0; break;
                    case 'up' :     
                        start - $scope.perPage >= 0 && (start = start - $scope.perPage);
                        break;
                    case 'down' :     
                        (start + $scope.perPage) < filteredData.length && (start = start + $scope.perPage);
                        break;
                    case 'bot' :     
                        start = filteredData.length-$scope.perPage;
                        break;
                    case 'half' :     
                        start = parseInt(filteredData.length/2);
                        break;
                    case 'alreadySet': return true;
                }
                return inStart != start || !start;
            }

            $scope.toPage = function(pgNbr) {
                start = (pgNbr -1) * $scope.perPage;
                console.log( pgNbr, start );
                $scope.setPage('alreadySet');
            }

            $scope.currPage = 1;
            $scope.setPage = function(upOrDown) {
                if (setStart(upOrDown)) {
                    shadowData = filteredData.slice(start, start + $scope.perPage);
                    $scope.listData = angular.copy(shadowData);
                    $scope.rowCount = filteredData.length;
                }

                $scope.currPage = parseInt((start+1)/$scope.perPage) + 1;
                $scope.totPages = parseInt((filteredData.length-1)/$scope.perPage) + 1;

                var pgList = [];
                for (var i=0; i<$scope.totPages; i++) pgList.push(i+1);
                $scope.pgList = pgList;
            }

            var ucType = attrs.listType.charAt(0).toUpperCase() + attrs.listType.slice(1);
            $scope.label = ucType;

            $scope.$watch('parentUid' , function(n, o) {
                if (typeof n != 'undefined') {
                    qString= attrs.parentUidField + '=' + $scope.parentUid;
                    $scope.refresh();
                }
            });

            $scope.refresh = function() {
                ready.refresh(attrs.listType, qString, function() {
                    loadData(qString);
                });
            }

            function loadData(qString) {
                typeof qString == 'undefined' && (qString='');
                $scope.loading  = 'fa-spin';
                $scope.allDiffs = false;
                if (typeof attrs.listType != 'undefined') {
                    $scope.ucType = ucType;

                    ready.get(attrs.listType, qString, function(data) {
                        if (typeof data.data != 'undefined') {
                            allData         = data.data[attrs.listType] == undefined ? [] : data.data[attrs.listType];
                            $scope.listSels = data.data.selects;
                            columns || (columns = data.data.columns);
                            $scope.listHeaders = columns;

                            if (dbCtrl == 'infra') //Generate array for which columns to hide on load
                                $scope.hidden = $scope.listHeaders.map(function(head) {
                                    if (!head.vis) return head.dbName;
                                });

                            $scope.runSearch();  // assign filteredData to allData

                            setTimeout( function() { $scope.reloadText = 'Reload'; $scope.$digest(); }, 400);
                            //shadowData = angular.copy($scope.listData);
                            setTimeout( function() {
                                $scope.loading = '';
                                $scope.$digest();
                            }, 800);

                            $scope.newRow = false;

                            $scope.delArray  = shadowData.map(function() { return false });
                            $scope.saveArray = shadowData.map(function() { return false });
                        }
                    });
                }
            }

            //typeof attrs.selectedName == 'undefined' && setTimeout(loadData, 10);
            //typeof attrs.selectedName == 'undefined' && loadData();
            typeof attrs.selectedName == 'undefined' && ready.register('users', loadData);

            $scope.shadow = [];

            $scope.isDiff = function(idx) {
                var newListData = angular.copy($scope.listData);
                delete newListData[idx].$$hashKey;

                for (var i in newListData[idx])
                    if (i == 'rssi') { newListData[idx].rssi = Number($scope.listData[idx].rssi); }

                $scope.saveArray[idx] = angular.toJson(newListData[idx]) != angular.toJson(shadowData[idx]) ? true : false;

                $scope.allDiffs = false;
                for (var i=0; i<$scope.saveArray.length; i++) 
                    if ($scope.saveArray[i]) {
                        $scope.allDiffs = true; 
                        break;
                    }
            }

            //ready.register(function() { shadowData= angular.copy($scope.listData); }, 1200);

            $scope.checkBtn = function(ctrlButton, lIdx) {
                return typeof attrs.ctrls == 'undefined' || attrs.ctrls.indexOf(ctrlButton) > -1;
            }

            $scope.clearDetail = function(idx) { el.find('detail').remove(); }

            function clearShadowDepr(lKey) {
                var map = [];
                $scope.shadow.map(function(a) {lKey == a || map.push(a); });
                $scope.shadow = map;
            }
            
            function getSingleUc(item) {
                item =  item.substring(item.length-1) == 's'
                            ? item.substring(0,item.length-1)
                            : item;
                return item.charAt(0).toUpperCase() + item.slice(1);
            }

            var delTimeout = null;

            $scope.delConfirm = function(arg) {
                clearTimeout(delTimeout);

                $scope.delArray[arg] = true;
                $scope.msg      = 'Cliquez la poubelle pour confirmer';

                /*delTimeout = setTimeout( function() {
                    $scope.msg == 'Cliquez ici pour confirmer' && ($scope.msg = '');
                    $scope.$digest();
                }, 8000);
                */
            }

            $scope.delItem = function (lIdx) {
                $scope.msg = '...suppression';
                if (lIdx == 0 && $scope.newRow == true) { $scope.newRow = false; }

                var params = $scope.listData[lIdx];
                clearTimeout(delTimeout);
                api.get(dbCtrl + '/del' + getSingleUc($scope.ucType), 'params=' + params.uid + '&' + qString, function( data ) {
                    if (typeof  data.name != 'undefined' && data.name == 'error')
                        $scope.msg = 'Delete user error';
                    else {
                        var item = $scope.ucType == 'Users' ? 'last' : 'name';
                        $scope.listData.splice(lIdx, 1);
                        $scope.delArray.splice(lIdx, 1);
                        $scope.saveArray.splice(lIdx, 1);

                        $scope.msg = 'Supprimé: ' + params[item];
                        shadowData = angular.copy($scope.listData);
                    }
                    delTimeout = setTimeout(function() { $scope.msg = ''; $scope.$digest();}, 3000);
                });
            };

            $scope.saveItem = function(lIdx) {
                $scope.msg  = 'enregistrement...';
                var params = $scope.listData[lIdx];
                if (lIdx == 0 && $scope.newRow == true) { $scope.newRow = false; }

                (typeof params.puid == 'undefined' || params.puid == '') && (params.puid = window.wrGlobal.user_uid);
                api.post(dbCtrl + '/save' + getSingleUc($scope.ucType), params, null, function(data) {
                    console.info( data, data.success );
                    /*
                    if (typeof  data.name == 'undefined' || data.name == 'error'
                        || typeof data.length == 'undefined' || data.length == 0
                    ) {
                        $scope.msg = 'Error or no data returned';

                    } else {
                        $scope.listData[lIdx].uid = data[0].uid;
                        var name = typeof params.name == 'undefined' ? params.first + ', ' + params.last : params.name;
                        $scope.msg = 'Saved : ' + name;
                        shadowData = angular.copy(data);
                    }
                    */
                    
                    $scope.msg = data.success ? 'Item Saved' : 'Error or no data returned';
                    if (!data.success) alert(JSON.stringify( data ));
                    setTimeout(function() { $scope.msg = ''; $scope.$digest();}, 3000);
                    //typeof cb == 'function' && cb(data);

                    //clearShadow(lIdx);
                    $scope.saveArray[lIdx] = false;
                    //shadowData = angular.copy($scope.listData);
                });
            }

            $scope.addMulti = function() {
                var params = [];
                for (var i=0; i<$scope.saveArray.length; i++)
                    if ($scope.saveArray[i])
                        params.push($scope.listData[i]);

                api.post(dbCtrl + '/saveMulti' + getSingleUc($scope.ucType), params, null, function(data) {
                    if (data.success) {
                        $scope.msg = 'Saved multiple items';
                        $scope.listData.map(function(d, i) { $scope.saveArray[i] = false; });
                        $scope.allDiffs = false;
                    } else {
                        $scope.msg = data.msg;
                    }
                });
            }

            $scope.add = function() {
                var cols = {};
                for (var i=0; i<$scope.listHeaders.length; i++) cols[$scope.listHeaders[i].dbName] = '';

                $scope.listData.unshift(cols);
                delete ($scope.listData[0].$$hashKey);
                shadowData = angular.copy($scope.listData);
                
                /*
                for (var i=0; i<$scope.shadow.length; i++)
                    $scope.shadow[i] = $scope.shadow[i] + 1;
                */

                $scope.saveArray.unshift(false);
                $scope.delArray.unshift(false);

                $scope.newRow = true;
            }

            $scope.selectField = function(field, vis) {
                if (vis)
                    $scope.hidden.splice($scope.hidden.indexOf(field), 1); 
                else
                    $scope.hidden.push(field); 
            }
        }
    }
}])
/*
.directive('detail', [function() {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: 'html/list/detail.html',
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('row', ['$compile', function($compile) {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: 'html/list/row.html',
        compile: function(el, attrs) {
            return function($scope, el, attrs) { // this is link function
                $scope.lastIdx = -1;

                $scope.detailItem = function(idx) {
                    $scope.clearDetail();
                    if ($scope.lastIdx == idx) {
                        $scope.lastIdx = -1;
                    } else {
                        $scope.lastIdx = idx;
                        el.append($compile(angular.element('<detail></detail>'))($scope));
                        $scope.detRows =  $scope.listData[idx];
                    }
                    $scope.blur(idx);
                }
            }
        }
    }
}])
.directive('usersRow', ['$compile', function($compile) {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: 'html/list/usersRow.html',
        compile: function(el, attrs) {
            return function($scope, el, attrs) { // this is link function
                $scope.lastIdx = -1;

                $scope.detailItem = function(idx) {
                    $scope.clearDetail();
                    if ($scope.lastIdx == idx) {
                        $scope.lastIdx = -1;
                    } else {
                        $scope.lastIdx = idx;
                        el.append($compile(angular.element('<detail></detail>'))($scope));
                        $scope.detRows =  $scope.listData[idx];
                    }
                    $scope.blur(idx);
                }
            }
        }
    }
}])
.directive('beaconsRow', ['$compile', function($compile) {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: 'html/list/beaconsRow.html',
        compile: function(el, attrs) {
            return function($scope, el, attrs) { // this is link function
                $scope.lastIdx = -1;

                $scope.detailItem = function(idx) {
                    $scope.clearDetail();
                    if ($scope.lastIdx == idx) {
                        $scope.lastIdx = -1;
                    } else {
                        $scope.lastIdx = idx;
                        el.append($compile(angular.element('<detail></detail>'))($scope));
                        $scope.detRows =  $scope.listData[idx];
                    }
                }
            }
        }
    }
}])
.directive('zonesRow', ['$compile', function($compile) {
    return {
        restrict: 'E',
        replace: false,
        templateUrl: 'html/list/zonesRow.html',
        compile: function(el, attrs) {
            return function($scope, el, attrs) { // this is link function
                $scope.lastIdx = -1;

                $scope.detailItem = function(idx) {
                    $scope.clearDetail();
                    if ($scope.lastIdx == idx) {
                        $scope.lastIdx = -1;
                    } else {
                        $scope.lastIdx = idx;
                        el.append($compile(angular.element('<detail></detail>'))($scope));
                        $scope.detRows =  $scope.listData[idx];
                    }
                }
            }
        }
    }
}])
*/
.directive('listButtons', ['$compile', 'TPL', function($compile, TPL) {
    return {
        restrict: 'E',
        replace: false,
        //templateUrl: 'html/list/buttons.html',
        template: TPL.list_buttons,
        link: function($scope, el, attrs) {
        }
    }
}])
/*
.directive('droppable', ['api', function(api) { 
    //<img src="./image/horizontal_scroll.png" style="width:1px; height:1px"></img>
    var crt = document.createElement('img');
    crt.src = './image/horizontal_scroll.png';
    crt.style.height='41px';
    crt.style.width='41px';
    crt.style.position='absolute';
    crt.style.top='-100px';
    crt.style.paddingLeft='18px';
    document.body.appendChild(crt);

    return {
        templateUrl : '',
        restrict: 'CA', 
        replace:true, 
        scope: true,
        link: function($scope, el) {
            var dragEl = null;
            el[0].addEventListener('dragover', function(e) { event.preventDefault(); });
            el[0].addEventListener('dragstart', function(e) { 
                e.dataTransfer.setDragImage(crt, 0, 0);
                dragEl = angular.element(e.target); 
            });

            function dropFn(e) {
                if (typeof $scope.onDrop == 'undefined') {
                    $scope.selZone($scope.zone.uid);
                    setTimeout(function() { $scope.$apply(); }, 100);
                } else  {
                    $scope.onDrop($scope, e);
                }
            }

            el[0].addEventListener('drop', dropFn);
        } 
    }
}])
*/
;
