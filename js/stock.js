APP
.directive('stockItem', ['FTPL', 'api','loginSvc', 'dragSvc', 'notifSvc', function(FTPL, api, loginSvc, dragSvc, notifSvc) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.stock_item,
        scope:false,
        link: function($scope, el) {
            $scope.edtme = false;

            var itemShadow = null;
            $scope.pencilClick = function(action) {
                //item = typeof item == 'undefined' ? {name: ''} : item;
                //console.log( ' item ' , item);
                $scope.nav.selItemName = $scope.item.name;
                if (action == 'undo') {
                    $scope.item = angular.copy(itemShadow);
                } else {
                    itemShadow = angular.copy($scope.item); 
                }

                //var keepStore = $scope.nav.edtme;

                $scope.edtme = !$scope.edtme;
                $scope.nav.edtme = $scope.edtme;

                dragSvc.init($scope, el, FTPL.stock_item_pop);
                $scope.nav.tagIdx = false;

                //$scope.setChecks($scope.item, $scope.edtme, keepStore);
                console.log( 'edtme', $scope.edtme , $scope.nav.edtme );
                $scope.edtme || $scope.chooseCats();
            }

            $scope.chooseCats = function(isClose) {
                console.log( isClose, 'is close' );
                isClose = typeof isClose != 'undefined' && isClose == 'close' ? false : $scope.edtme;
                $scope.setChecks($scope.item, isClose, false);
            }

            $scope.imgClicked = function(action, val) {
                //var keepStore = $scope.nav.edtme;
                $scope.nav.selItemName = val ? $scope.item.name : '';

                if (action == 'save') {
                    $scope.saveItem();
                } else {
                    $scope.nav.tagIdx = false;
                    //$scope.setChecks($scope.item, val, keepStore);
                    $scope.nav.edtme = val;
                    $scope.edtme = false;
                }
            }

            $scope.saveItem = function() {
                $scope.nav.tagIdx && ($scope.item.tags = $scope.nav.tagIdx); 

                api.post('stock/saveItem', $scope.item, function(itemResp){
                    notifSvc.ok('Saved', 5);
                    dragSvc.init($scope, el, FTPL.stock_item_pop);
                    $scope.item.uid = itemResp.data[0].uid;
                });
            }

            $scope.delItem = function() {
                notifSvc.warn('Delete this item?', 5, function(yN) {
                    yN && api.get('stock/delItem', 'uid=' + $scope.item.uid, function(itemResp){
                        $scope.searchContent();
                    });
                });
            }

            $scope.$watch('stock', function(n, o) {
                n == 'undefined' || (n[0].name == '' && $scope.pencilClick());
            });
        }
    }
}])
.directive('stockPgr', ['FTPL', 'api','loginSvc', '$compile', function(FTPL, api, loginSvc, $compile) {
    return {
        restrict: 'A',
        replace:  true,
        scope:    false,
        link: function($scope, el) {
            $scope.pages = [1,2,3];
            el.append($compile(FTPL.pgr)($scope));
        }
    }
}])
.directive('stock', ['FTPL', 'api','loginSvc', '$location', 'dragSvc', function(FTPL, api, loginSvc, $location, dragSvc) {
    return {
        restrict: 'A',
        replace: true,
        template: FTPL.stock_main,
        scope:false,
        link: function($scope, el) {
            $scope.checks = {};
            (typeof $scope.nav == 'undefined' || $scope.nav == null) && ($scope.nav = {});
            $scope.nav.edtme = false;
            var catKeys = [];

            typeof $scope.nav == 'undefined' && ($scope.nav = {});
            $scope.nav.selItemName = '';

            $scope.pencilClick = function(val) {
                $scope.edtme = !$scope.edtme;
                dragSvc.init($scope, el, FTPL.stock_manage_pop, {topRight: true});
            }

            $location.search().rt == 'cat' && $scope.pencilClick();
            console.log( $location.search().rt , ' stock location');

            $scope.newCatItem = {};
            $scope.addCategory = function() {
                $scope.cats['9999'] = {name:'', items: {}};
                $scope.newCategory = true;
            }

            $scope.newCatItem['9999'] = true;
            $scope.addCatItem = function(catName) {
                var cats = angular.copy( $scope.cats );
                for (var i in $scope.cats) {
                    if ( $scope.cats[i].name == catName ) {
                        $scope.cats[i].items['9999'] = '';
                        $scope.newCatItem[i] = true;
                        break;
                    }
                }
            }

            function saveCat(name, catId, cb) {
                api.get('stock/saveCat', 'name=' + name + '&cat_id=' + catId, function(resp) {
                    $scope.newCategory = false;
                    typeof cb == 'function' && cb(resp);
                });
            }

            function saveCatItem(catId, itemName, itemId, cb) {
                api.get('stock/saveCatItem', 'name=' + itemName + '&cat_mask=' + catId + '&item_id=' + itemId, function(resp) {
                    $scope.newCatItem[catId] = false;
                    typeof cb == 'function' && cb(resp);
                });
            }

            $scope.saveCat = function(name, catId) {
                saveCat(name, catId, function(resp) {
                    console.log( resp, ' resp '  );
                    $scope.cats = genCats( resp );
                });
            }

            $scope.saveCatItem = function(catId, itemName, itemId) {
                if (catId == '9999') {
                    saveCat(catName, function(catResp) {
                        saveCatItem(itemName, catId, function(catResp) {
                            $scope.cats = genCats( resp );
                        });
                    });
                } else {
                    saveCatItem(catId, itemName, itemId, function(catResp) {
                        console.log( catResp, ' cat resp ' );
                        $scope.cats = genCats( catResp );
                    });
                }
            }

            $scope.delCategory = function(catId) {
                api.get('stock/delCat', 'mask=' + catId, function(resp) {
                    $scope.cats = genCats( resp );
                    $scope.newCatItem[catId] = false;
                });
            }

            $scope.delCatItem = function(catId, itemId) {
                api.get('stock/delCatItem', 'mask=' + itemId + '&cat_mask=' + catId, function(resp) {
                    $scope.cats = genCats( resp );
                    $scope.newCatItem[catId] = false;
                });
            }

            function loadStock() {
                api.post('stock/getStock', 
                    {checks: $scope.checks, contentSearch: $scope.contentSearch}, function(stockResp) {
                    $scope.stock = stockResp.data;
                    $scope.pgrAction('init', function(data) {$scope.stock = data;}, 'stock');
                    $scope.nav.currPage = 1;
                });
            }

            $scope.searchContent = loadStock;

            function genCats(cats) {
                for (var i in cats) {
                    $scope.checks[i] = {};
                    catKeys.push(i);

                    if (Object.keys(cats[i].items)[0] === 'null')
                        delete  cats[i];
                    else
                        for (var j in cats[i].items)
                            cats[i].items[j] == 'null' && ($scope.checks[i][j] = false);
                }
                return cats;
            }

            function load() {
                api.get('stock/getDefs', '', function(stockResp) {
                    $scope.cats = genCats(stockResp.data);
                    loadStock();
                });
            }
            load();

            $scope.searchChanged = function() {
                if ($scope.nav.edtme) {
                    $scope.nav.tags   = [];
                    $scope.nav.tagIdx = {};

                    for (var i in $scope.checks){
                        if (typeof $scope.cats[i] != 'undefined') {
                            for (var j in $scope.checks[i]) {
                                if ($scope.checks[i][j]) {
                                    $scope.nav.tags.push($scope.cats[i].name + '/' + $scope.cats[i].items[j]);
                                    typeof $scope.nav.tagIdx[i] == 'undefined' && ($scope.nav.tagIdx[i] = {});
                                    $scope.nav.tagIdx[i][j] = 0;
                                }
                            }
                        }
                    }
                } else {
                   loadStock();
                }
            }

            $scope.addItem = function() {
                console.log( $scope.stock[0].name, ' name ' );
                $scope.stock[0].name 
                   ?  $scope.stock.unshift({name:'', img:'./image/noimage.png'})
                   :  $scope.stock.shift();
            }

            var checkStor = '{}';
            $scope.setChecks = function(item, edtme, keepStore) {
                console.log( ' IN SET CHECKS', edtme, keepStore );

                if (edtme) {
                    typeof item.tags == 'undefined' && (item.tags = {});
                    keepStore || (checkStor = JSON.stringify($scope.checks));
                    for (var i in $scope.cats){
                        var ck = typeof item.tags[i] == 'undefined' ? {} : item.tags[i];
                        for (var j in $scope.cats[i].items) {
                            //console.log($scope.checks[i]);
                            $scope.checks[i][j] = typeof ck[j] != 'undefined';
                        }
                    }
                    $scope.poppedOut = true;
                } else {
                    $scope.poppedOut = false;
                    $scope.checks = JSON.parse(checkStor);
                    $scope.nav.inChecks = false;
                }
            }
        }
    }
}])
