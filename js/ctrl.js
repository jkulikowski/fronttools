INFO=console.info;
WARN=console.warn;
STR=JSON.stringify;

APP
.config(['$routeProvider', 'TPLProvider', 'deviceDetectorProvider', function($routeProvider, TPLProvider, deviceDetectorProvider) {

    var routes = {};
    ['login', 'settings', 'land'].map(function(ctrl){
        routes[ctrl] = { template: TPLProvider.$get()['ctrl_' + ctrl], controller: ctrl };
    });

    if (!deviceDetectorProvider.$get().isMobile()) {
        //routes.files.template = TPLProvider.$get()['files_fileManager'];
    }

    $routeProvider
        .when("/",              routes.land)
        .when("/land",          routes.land) 
        .when("/settings",      routes.settings) 
        .when("/login",         routes.login) 
    ;
}])
.directive('contentRouter', ['ready', 'deviceDetector', 'idbSvc', 'swSvc', 'LS', 'loginSvc','api', 'notifSvc',function(ready, deviceDetector, idbSvc, swSvc, LS, loginSvc, api, notifSvc) {
    return {
        restrict: 'AC',
        replace: false,
        template:   '<div ng-view></div>' +
                    '<button ng-click="fromLs()">Load Unsaved</button>' +
                    '<div options></div>',
        link: function($scope, el, attrs) {
            notifSvc.activate( $scope );
            function setData(resp, cb) {
                typeof localStorage.stock == 'undefined' || (lsStock = JSON.parse(localStorage.stock));
                lsStock.map(function(lss) { lsUidStock[lss.uid] = lss; });

                if (typeof resp != 'undefined') {    
                    stock           = resp.data.items;
                    chartData       = resp.data.chart;
                    $scope.datain   = angular.copy( chartData );
                } else 
                    stock = lsStock;
                $scope.stock = angular.copy(stock);

                stock.map(function(s) {
                    lsUidStock[s.uid].used  && (s.used = lsUidStock[s.uid].used);
                    lsUidStock[s.uid]['order'] && (s.order= lsUidStock[s.uid]['order']);
                    lsUidStock[s.uid].restock && (s.restock = lsUidStock[s.uid].restock);

                    s.warn = s.stock<= s.thresh_min;
                    var firstL =  s.product_name.substring(0,1).toUpperCase();
                    $scope.letters.indexOf(firstL) == -1 && $scope.letters.push(firstL);
                });
                $scope.filter('ALL');

                typeof cb == 'function' && cb();
            }

            // LST START
            $scope.getItems = function(cb) { 
                api.get('stock/getItems', [], function(resp) { setData(resp, cb); });
                // $scope.stock = resp.data.items;
            }

            $scope.delItem  = function( item ) { api.get('stock/delItem', 'uid=' + item.uid, setData); }
            $scope.saveItem = function(item) {
                api.post('stock/saveItem', item, function(resp) {
                    $scope.getItems();
                });
            }

            $scope.editItem = function(item) {
                $scope.edItem = item; 
            }

    
            $scope.viewItem = function(item) {
                $scope.stockTitle = [item.product_name]
                $scope.stock = stock.filter(function(s) {
                    return s.product_name.trim() == item.product_name.trim();
                });
                
                $scope.datain = chartData.filter(function(d ,i) {
                    return i==0 || item.product_name.trim() === d[0].trim();
                });
            }

            $scope.stockAction = function(typ, item) {
                $scope[typ + 'Item'](item);
            }
            // LST END

            function setItemTypVal(item, typ) {
                for (var i=0; i<stock.length;        i++) if (item.uid==stock[i].uid)        break;
                for (var j=0; j<$scope.stock.length; j++) if (item.uid==$scope.stock[j].uid) break;
                stock[i][typ] = $scope.stock[j][typ];

                return stock;
            }

            $scope.toLs = function(item, typ) {
                localStorage.stock = JSON.stringify( setItemTypVal(item, typ) );
            }

            $scope.fromLs = function() {
                stock = JSON.parse(localStorage.stock);
                $scope.stock = angular.copy( stock );
            }


            $scope.stockTitle = ['stock title'];
            $scope.filter = function(l, typ) {
                $scope.stockTitle = l == 'ALL' ? ['Low Stock Items'] : [l + ' items'];
                switch (l) {
                    case 'ALL' :  $scope.stock = angular.copy(stock); break;
                    case '+'   : 
                        $scope.stock = stock.filter(function(s) { return s[typ]; });
                        break;
                    default:
                        $scope.stock =  stock.filter(function(s) {
                            return s.product_name.substring(0,1).toLowerCase() == l.toLowerCase();
                        });

                }

                $scope.datain = chartData.filter(function(d ,i) {
                    return i == 0 ||    l == 'ALL'
                                            ? d[1] <= d[2]
                                            : l.toLowerCase() === d[0].substring(0,1).toLowerCase();
                    });
            }

            $scope.mkChart = function(typ) {
                var active = $scope.stock
                    .filter(function(s) { return s[typ]; })
                    .map(   function(s) { return [s.product_name, s[typ]]; });

                active.unshift( ['Item','Count'] );
                return active;
            }

            // ------------------- STOCK -------------------


            if (typeof localStorage['jkauth'] != 'undefined' && localStorage.jkauth ) {
                $scope.userName = JSON.parse(localStorage['jkauth']).userName;
                $scope.passwd   = JSON.parse(localStorage['jkauth']).passwd;
            }

            $scope.login = 'tuc';
            $scope.passwd = 'conkey';

            loginSvc.login($scope, function(loginData) {
                //console.log( 'LOGIN CTRL', loginData);
            }); 

            var filesHref = window.location.protocol + '//' + window.location.host + '/files/';

            $scope.isMobile     = deviceDetector.isMobile();
            $scope.testMob      = true;
            $scope.users        = [];
            $scope.clients      = [];

            $scope._setScopeVar = function(handle, data) { $scope[handle] = data; }
            $scope.refreshData  = function(whichSet)     { loads.get[whichSet];   }

            var loads = { 
                setPaths: function(data) {
                    for (var key in data)
                        data[key].files.map(function(d) { 
                            d.path = filesHref + d.path + '/' + d.name; 
                        });
                    return data;
                },
                get: {
                    /*
                    users: function() {
                        ready.get('clients', 'puid=' + window.wrGlobal.user_uid, function(userData) {
                            //console.info( userData.success, ' users data ' );

                            if (userData.success) {
                                $scope.clients = userData.data.users.filter(function(u) {
                                    return u.l = u[u.last == '' ? 'email' : 'last'].substring(0,1).toUpperCase();
                                }).sort(function(i,j) { return i.l<j.l ? -1 : 1; });

                                $scope.showAllUsers = $scope.clients.length < 12;

                                $scope.cli = {};
                                $scope.clients.map(function(u) {
                                    typeof $scope.cli[u.l] == 'undefined' && ($scope.cli[u.l] = []);
                                    $scope.cli[u.l].push(u);
                                });

                                console.log( $scope.cli, ' cLI'  );
                            }
                        }, true);
                    },
                    files: function () {
                        ready.get('files', function(dirs) {
                            $scope.myFiles = loads.setPaths(dirs.data);
                        });
                        ready.get('sharedFiles', function(dirs) {
                            $scope.sharedFiles = loads.setPaths(dirs.data);
                        });
                    },
                    notifs: function() {
                        ready.get('notifsForMe', function(notifs) {
                            $scope.notifLogForMe = notifs.data;
                        });
                        ready.get('notifsFromMe', function(notifs) {
                            $scope.notifLogFromMe = notifs.data;
                        });
                    }
                    */
                }
            }

            //ready.register('clients',   loads.get.users);
            //ready.register('files',     loads.get.files);
            //ready.register('notifs ',   loads.get.notifs);

            // service Worker START
        }
    }

}])
;

APP
.controller('login', ['$scope', 'ready', 'api', function($scope,ready, api) {
        $scope.login = 'tuc';
        $scope.passwd = 'passwd';
}])
.controller('used', ['$scope', 'ready', 'api', function($scope,ready, api) {
    $scope.typ = 'used';
    $scope.stock || setTimeout($scope.getItems, 500);

    $scope.usedChart = [[]];
    $scope.usedTitle = ['Used Items'];

    $scope.markUsed = function() {
        $scope.usedChart = $scope.mkChart($scope.typ);
    }

    setTimeout(function() {$scope.markUsed(), $scope.$$phase || $scope.$digest() }, 1000);
}])
.controller('order', ['$scope', 'ready', 'api', function($scope,ready, api) {
    $scope.typ = 'order';
    $scope.stock || setTimeout($scope.getItems, 500);

    $scope.orderChart = [[]];
    $scope.orderTitle = ['Ordered Items'];

    $scope.markOrdered = function() {
        $scope.orderChart = $scope.mkChart($scope.typ);
    }

    setTimeout(function() {$scope.markOrdered(), $scope.$$phase || $scope.$digest() }, 1000);
}])
.controller('land', ['$scope', function($scope) {
    var fullOn = false;

    $scope.goToFullScreen = function() {
        

//navigator.bluetooth.requestDevice({ filters: [{ services: ['battery_service'] }] })
navigator.bluetooth.requestDevice()
.then(function(){ console.log('battery service here',arguments); })
.catch(function(error)  { console.log(error, ' error on bluetooth'); });

        return false;
        var bodyEl = document.getElementsByTagName('body')[0];
        console.log( bodyEl ,' body el');
            fullOn = !fullOn;

        //bodyEl.addEventListener('click', function(el) {
            //bodyEl.removeEventListener('click', function() { console.log( arguments , ' REMOvE');});
            console.log( Document.webkitExitFullscreen);
            console.log( document.webkitExitFullscreen);
            console.log( document.webkitRequestFullscreen);
            //console.log( ' clicked on body ',el.target );
            if (fullOn) {
                typeof bodyEl.webkitExitFullscreen == 'undefined' || bodyEl.webkitExitFullscreen();
                    document.webkitExitFullscreen();
                              //webkitExitFullscreen

                //document.webkitExitFullscreen();
                //document.exitFullscreen();
            }else {
                //document.webkitRequestFullscreen();
                //document.requestFullscreen();
                bodyEl.webkitRequestFullscreen();
            }
            //this.webkitRequestFullscreen();
        //});
        console.log('is in full', fullOn);
    }
}])
.controller('files', ['$scope', 'api', 'ready', 'api', '$http', '$location', '$anchorScroll', function($scope, api, ready, api, $http, $location, $anchorScroll) {
    var href = window.location.protocol + '//' + window.location.host + '/files/';

    $scope.uploadStatus  = '';
    $scope.showClientSel = true;

    $scope.selTab = typeof $location.$$search == 'undefined' ? 'mine' : $location.$$search.owner;

    // Data Load ---START---
    function loadUserFiles(cb) {
        api.get('files/getDirs', function(dirs) {
            for (var key in dirs.data)
                dirs.data[key].files.map(function(d) { d.path = href + d.path + '/' + d.name; });
            $scope.myFiles = dirs.data;

            typeof cb == 'function' && cb();
        })
    }

    function loadSharedFiles(cb) {
        api.get('files/getSharedDirs', function(dirs) {
            for (var key in dirs.data)
                dirs.data[key].files.map(function(d) { d.path = href + d.path + '/' + d.name; });
            $scope.sharedFiles = dirs.data;
            typeof cb == 'function' && cb();
        })
    }

    $scope.refreshCl = 'fa-refresh';
    $scope.reloadFiles = function() { 
        $scope.refreshCl = 'fa-refresh fa-spin';
        loadUserFiles();
        loadSharedFiles( function() {
            $scope.refreshCl = 'fa-refresh';
            $scope.$$phase || $scope.$digest();
        });
    }
    // Data Load ---END---

    $scope.selFile = -1;
    $scope.setSelFile = function(idx, dir, $event) { 
        if ($scope.selFile == idx) {
            $scope.selFile = -1;
        } else {
            $scope.selDir       = dir; 
            $scope.selFile      = idx;
            $scope.currentFile  = $scope.selTab == 'mine' ? $scope.myFiles : $scope.sharedFiles;
            $scope.currentFile  = $scope.currentFile[$scope.selDir].files[idx];
        }

        false && setTimeout( function() {
            var thisEl = el;
            var bodyEl = angular.element(document.body)[0];
            //var bodyAt = document.body)[0].offsetHeight - document.body)[0].scrollTop;
            //console.log( bodyEl.scrollTop , $event.pageY, ' pp');
            //scrollVal = bodyEl.offsetHeight - ($event.pageY - bodyEl.scrollTop);
            //scrollVal = $event.pageY - bodyEl.scrollTop;
            //console.log( scrollVal , 'scroll val');
            if ($event.pageY-bodyEl.scrollTop > 340)
                document.body.scrollTop = 160 + bodyEl.scrollTop;
        }, 100);

        var bodyEl = angular.element(document.body)[0];
        console.log('f_' + dir + '_' + idx);
        $anchorScroll('f_' + dir + '_' + idx);
        var ell = document.getElementById('f_' + dir + '_' + idx);
        var lc =  document.getElementById('listContent');
        //var par = ell.parent;
        //console.log( angular.element(par) , ' par ');
        setTimeout( function() {
            var off =   ell.offsetTop +
                        ell.offsetParent.offsetTop +
                        ell.offsetParent.offsetParent.offsetTop - 50;
            //document.body.scrollTop =  document.body.scrollTop +  off;
            if (off < 0 ) off =0;
            //console.log( ell.offsetTop, ' offset top ', off , lc.scrollTop, ' lc  top', angular.element(lc));
            //document.body.scrollTop = off;
            //document.body.scrollTop = 0;
            lc.scrollTop = off;
            lc.offsetTop = off;
            //console.log( ' lc  top', lc.offsetTop, lc.scrollTop );
            //window.scrollTo(0, off);
            //console.log( ' done ' );

        }, 1200);

    }

    $scope.selDir = '';
    //$scope.dirIcon = 'fa-folder-o';
    $scope.dirIcon = 'fa-pencil';

    $scope.setSelDir = function(idx) { 
        idx == $scope.selDir && (idx = '');
        $scope.selDir  = idx; 
        $scope.selFile = -1;
    }

    // Dirs ---START---
    $scope.dirPop = false;
    var oldDir = '';
    $scope.dir = '';
    
    $scope.renameDir = function(dir, e) {
        e && e.stopPropagation(); 

        $scope.newDirName = '';
        $scope.dir        = dir;
        oldDir            = dir;
        $scope.dirPop     = true;
        $scope.setFocus();
    }

    $scope.addDir = function() {
        $scope.dir      = '';
        $scope.dirPop   = true;
        $scope.setFocus();
    }

    function updateDirName() {
        var qString = 'dir_uid=' + $scope.myFiles[oldDir].uid + '&new_name=' + $scope.newDirName + '&old_name=' + oldDir;
        api.get('files/renameDir', qString,  function(resp) {
            if (resp.success) {
                loadUserFiles();
            } else {
                alert('rename didnt work', JSON.stringify(resp));
            }
        });
    }

    function createDir() {
        api.get('files/createDir', 'dirName=' + $scope.newDirName, function(data) {
            if (data.success) {
                loadUserFiles();
            } else {
                alert('directory creation failed');
            }
        });
    }

    $scope.changeDir = function(isCancel) {
        $scope.dirPop = false;
        if (isCancel !== true) {

            if ($scope.dir != $scope.newDirName) {
                if ($scope.dir == '') createDir();
                else updateDirName();
            }
        }
    }
    // Dirs --- END ---

    $scope.preview = function(idx, dir) {
        $scope.setSelFile(idx, dir);
        var path =  $scope.currentFile.path;

        $scope.isPdf = path.substring(path.length-3).toUpperCase() == 'PDF'; 
        
        $scope.openFrameWrap = !$scope.isMobile || ($scope.isMobile && !$scope.isPdf);
        $scope.previewUrl    = $scope.currentFile.path;
    }

    $scope.fileUp = false;
    $scope.fileUpload = function(dir, e) {
        e && e.stopPropagation();
        $scope.fileUp = $scope.myFiles[dir];
        $scope.fileUp.name= dir;
    }

    $scope.uploadFileDepr = function() {
         rdr.onload = function(){  console.log( arguments ) };
         console.log( ' args ', arguments );
    }

    $scope.uploadFile = function(files) {
        var loaded = 0;
        var rdr = new FileReader();
        var file = files[0];

        $scope.uploadStatus = 'fa-refresh fa-spin';
        rdr.onload = function() {
            var o = {
                file:       rdr.result,
                dirName:    $scope.fileUp.name,
                dirUid:     $scope.fileUp.uid, 
                fileName:   file.name,
                fileType:   rdr.result.substring(5,9),
                ext:        'NOT DAT'
            };

            var uploadUrl = 'files/uploadFile';
            $http.post(uploadUrl, o)
            .success( function() { 
                $scope.fileUp = false;
                loadUserFiles();
                $scope.uploadStatus = 'fa-check';
                setTimeout( function() {
                    $scope.uploadStatus = '';
                    $scope.$$phase || $scope.$digest();
                }, 3000);
            })
            .error(function() { 
                console.log( arguments,' error upload'); 
            });
        };

        rdr.readAsDataURL( file );
    }

    $scope.openSharedUsers = function(dir, idx, e) {

        var fileAccess = $scope.myFiles[dir].files[idx].access;
        fileAccess || (fileAccess=[]);
        var shared = fileAccess.map(function(a) { return a.f1; });
        $scope.shareClients = $scope.clients.filter( function(c) { 
            c.shared = shared.indexOf(c.uid) > -1;
            c.icon   = c.shared ? 'fa-close' : 'fa-plus';
            return true;
        });

        $scope.underLayer = true;
        if (idx == $scope.selFile) 
            $scope.selFile || e.stopPropagation();

        $scope.showUsers = 'active';
    }

    function shareClients(idx, dir) {
        var fileAccess = $scope.myFiles[dir].files[idx].access;
        fileAccess || (fileAccess=[]);
        var shared = fileAccess.map(function(a) { return a.f1; });
        $scope.shareClients = $scope.clients.filter( function(c) { 
            c.shared = shared.indexOf(c.uid) > -1;// ? 'no' : 'yes';
            c.icon = c.shared ? 'fa-close' : 'fa-plus';
            return true;
        });
    }

    function shareFile(user_uid, file_uid, client) {
        var action      = client.shared ? 'revoke' : 'grant';
        var url         = 'files/' + action +  'Access';
        var queryString = 'file_uid=' + file_uid + '&user_uid=' + user_uid;

        client.icon = 'fa-refresh fa-spin';
        api.get(url, queryString, function(resp) {
            $scope.selClient = null;
            loadUserFiles(
                function() {    
                    shareClients($scope.selFile, $scope.selDir) 
                }
            );
        });
    }

    $scope.slideOut = function() {
        $scope.showUsers    = 'slideOut';
        $scope.underLayer   = false;
    }

    $scope.shareFile = function(client, e) {
        e.stopPropagation();
        $scope.selClient = client;

        var file_uid = $scope.myFiles[$scope.selDir].files[$scope.selFile].uid;
        var user_uid = client.uid;

        if ( $scope.isMobile && e.type=='touchend') {
            var touch = $scope.touchEnd(e);

            if (!touch.swipeX && !touch.swipeY)
                shareFile(user_uid, file_uid, client);
        } else {
            e.type=='touchend' || shareFile(user_uid, file_uid, client);
        }
    }

    $scope.emailto      = '';
    $scope.emailStatus  = 'fa-send';
    $scope.mailFile = function(idx, file, emailto) {
        if (emailto) {
            var queryString = 'file_uid=' + file.uid + '&user_email=' + emailto;
            api.get('files/mailFile', queryString, function(resp) {
                console.log( ' mailed file ' , resp );
                $scope.emailto = '';
                $scope.emailStatus = 'fa-check';
                setTimeout( function() {
                    $scope.emailStatus = '';
                    $scope.$$phase || $scope.$digest();
                }, 3000);
            });
        }
    }

    function getClientByUid(uid) {
        return $scope.clients.filter(function(c) {
            return uid == c.uid;
        })[0];
    }

    $scope.viewPdfDepr = function() {
        var href = 'http://dev.bmr.webroad66.com/files/Jarek/Benny&Co%20Estimate%20-%20Sheet1.pdf';
        console.log( href );
    }

    $scope.delFile = function(idx, dir){
        var file = $scope.myFiles[dir].files[idx];
        var url  = 'files/delFile?fileName=' + file.name + '&dirName=' + dir + '&uid=' + file.uid;
        
        if (confirm('Delete file: \n\t' + file.name + '\n\nplease confirm'))
            api.get(url,'', function(resp) {
                console.log( resp );
                if (resp.success) {
                    loadUserFiles();
                    $scope.selFile = null;
                }
            });
    }

    $scope.fetchDepr = function(token) {
    console.log( ' TOKEN', token );
        $.ajax({
          url: 'https://www.google.com/m8/feeds/contacts/default/full?alt=json',
          dataType: 'jsonp',
          data: token
        }).done(function(data) {
            console.log(JSON.stringify(data));
        });
    }
}])
.controller('users', ['$scope', 'ready', '$http', '$location', function($scope, ready, $http, $location) {
    $scope.selIdx  = -1;
    $scope.myPerms = window.wrGlobal.perms;

    $scope.resetPasswd = function(idx) {
        alert( 'New Password for User ' + $scope.clients[idx].first + ' ' 
            + $scope.clients[idx].last +'\nhas been sent to email\n\n' 
            + $scope.clients[idx].email );
    }

    $scope.$watch('loggedIn', function(n, o) {
        $scope.hasAdminAuth = window.wrGlobal.perms != 'CLI';
    });

    $scope.saveUser = function(idx) {
        if (idx==0 && $scope.clients[idx].uid == null) {
            $scope.clients[0].puid = window.wrGlobal.user_uid;
            $http({url:'infra/saveClient', method:'POST', data: $scope.clients[0]}).
            then(function(returned){
                $scope._setScopeVar('clients', returned.data.data.users);
            });
        } else {
            $http({url:'infra/saveUser', method:'POST', data: $scope.clients[idx]}).
            then(function(returned){
                $scope._setScopeVar('clients', returned.data.data.users);
            });
        }
        $scope.selIdx = -1;
        $scope.editClass = 'fa-pencil';
    }

    $scope.delClient = function(idx) {
        var uid = $scope.clients[idx].uid;
        $http({url:'infra/delClient?client_uid=' + uid, method:'get'})
        .then(function(returned){
            $scope._setScopeVar('clients', returned.data.data.users);
            $scope.selIdx = -1;
        });
    }

    $scope.addClient = function() {
        console.log( $scope.clients[0].uid ,' first uid ');
        if ($scope.clients.length == 0 || $scope.clients[0].uid !== null) {
            $scope.clients.unshift({uid:null, first:'', last:'', email:'', perms: 'CLI'});
            $scope.selIdx=0;
            setTimeout( function() {
                var firstInput =  el.find('input')[0];
                firstInput.focus();
            }, 300);
        } else {
            $scope.clients.shift();
            $scope.selIdx = -1;
        }
    }
    
    $scope.editClass = 'fa-pencil';
    $scope.editClient = function(idx, client) {
        if (idx == 0 && $scope.clients[0].uid == null) $scope.clients.shift();

        $scope.selClient = client;
        $scope.selIdx = $scope.selIdx == idx ? -1 : idx;
        $scope.editClass =$scope.selIdx == idx ? 'fa-check':  'fa-pencil';
    }

    $scope.showMore = false;
    $scope.moreFields = function() { $scope.showMore = !$scope.showMore }
    $scope.changeEmail = function(idx) {
        console.log( 'show more', $scope.showMore );
        if ($scope.clients[idx].uid == null && !$scope.showMore ) {
            $scope.clients[idx].first = $scope.clients[idx].email;
            $scope.clients[idx].login = $scope.clients[idx].email;
        }
    }
}])
.controller('login', ['$scope','loginSvc', 'notifSvc', function($scope, loginSvc, notifSvc) {
    notifSvc.info('login');

    $scope.login  = function() { 
        console.error( 'calling login ');
        loginSvc.login($scope, function(loggedIn) {
            console.log( 'LOGIN CTRL', loggedIn );
        }); 
    }
    $scope.logout = function() { loginSvc.logout($scope); }


    var wrCreds = {
        "web": {
            "client_id": "1035165861134-v2q106kf4jgguhl10vlikmpvfq67mlh6.apps.googleusercontent.com",
            "project_id": "wr66-push",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://accounts.google.com/o/oauth2/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            "client_secret": "cxeV6qUXI6NSHvdKTLB6yRnj",
            "javascript_origins": ["https://dev.jktools.pro"]
        }
    };

    var CLIENT_ID = wrCreds.web.client_id;

    var SCOPES = [];
    SCOPES = ["https://www.googleapis.com/auth/calendar.readonly",'https://www.googleapis.com/auth/gmail.readonly' ];

}])
.controller('settings', ['$scope', 'notifSvc', function($scope, notifSvc) {
}])
;
