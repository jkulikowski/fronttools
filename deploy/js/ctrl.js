INFO=console.info;
WARN=console.warn;
STR=JSON.stringify;

APP
.directive('contentRouter', ['TPL', 'deviceDetector', 'LS', 'loginSvc','api', 'notifSvc', 'fileSvc', function(TPL, deviceDetector, LS, loginSvc, api, notifSvc, fileSvc) {
    return {
        restrict: 'AC',
        replace: false,
        template: TPL.ctrl_router,
        link: function($scope, el, attrs) {
            $scope.nav = {hideSave: false};

            notifSvc.activate( $scope );
            var myHost = document.location.hostname;
            document.title = myHost.split('.').shift().toUpperCase();
            $scope.nav.https = document.location.protocol == 'https:';

            $scope.isLogin = document.location.hash.indexOf('login') > -1;
            $scope.auth = {login: 'tuc', passwd: 'conkey', loginChecked: true, loggedIn: false, pop:false};
            switch (document.title) {
                case 'CLEAN': $scope.auth.login = 'clean'; $scope.passwd='test'; break;
                case 'LOLA':  $scope.auth.login = 'lola';  $scope.passwd='test'; break;
            }

            var emptyInits = { links: [[]], staff: [], sideList:[], locs:[] }
            $scope.RT = {};

            var inits = {
                links : {link:'', linkText:''},
                staff:  {img: '', head: '', text: '', link: '', linkText: ''},
                slideList: {},
                locs: {hours: {mo: {}, tu: {}, we: {}, th: {}, fr:{}, sa: {}, su:{}}, cat:{}}
            };

            function addInits() {
                ['links', 'staff', 'slideList', 'locs'].map(function(i) {
                    if (typeof $scope.cms[i] == 'undefined' || !$scope.cms[i])
                        $scope.cms[i] = emptyInits[i];
                });
            }

            $scope.dropped= function(dragEl, dropEl) {
                console.log('dropped', arguments );
            }

            var lsCms = LS('cms.content', null, 'CMS');

            $scope.saveCms = function(cb) {
                api.post('cms/save', $scope.cmsData, function(resp) {
                    console.log( resp, ' resp ' );
                    if (resp.success) {
                        cb(true);
                    }
                    else { cb(false); }
                });
            }

            typeof lsCms == 'undefined' && ($scope.cms = lsCms);
            var host = location.host.split('.')[0];
            function loadCms(cb) {
                api.get('./js/gen/cms.json', function(resp){ 
                    if (typeof lsCms == 'undefined' || lsCms.ts < resp.ts ) {
                        LS('cms.content', resp, 'CMS');
                        $scope.cms = resp; 
                    } else {
                        $scope.cms = resp; 
                    }

                    //addInits();
                    typeof cb == 'function' && cb();
                });
            }
            loadCms();

            $scope.popLogin = function() { WARN( 'popLogin'); $scope.auth.pop = true; }

            $scope.addOne = function(typ, idx) {
                typeof idx == 'undefined' 
                    ? $scope.cms[typ].push(inits[typ])
                    : $scope.cms[typ][idx].push(inits[typ]);
            }

            $scope.doEdit = function(isCancel) {
                isCancel == typeof isCancel != 'undefined' && isCancel;

                $scope.edt = !$scope.edt;
                if ($scope.edt || isCancel) {
                    loadCms(function() {
                        $scope.cmsData = { uid: $scope.cms.uid, host: myHost, cms: $scope.cms };
                    });
                } else  { //save
                    $scope.cms.ts = new Date().getTime();
                    api.post('cms/save', $scope.cmsData, function(resp) {
                        console.log( resp, ' resp ' );
                    });
                }
            }
            
            var files = null;
            $scope.auth.loginNotFound = false;
            $scope.sendLogin = function() {
                loginSvc.login($scope, function(resp) {
                    if (resp.success) {
                        $scope.auth.loggedIn = resp.success;
                        $scope.auth.pop = false;
                        fileSvc.loadFiles();
                        $scope.doEdit();
                        console.log( "LOGIN ", resp );
                    } else {
                        notifSvc.warn('Wrong password or account name. Would you like to register?', 4, function(yN) {
                            console.log(yN, 'yN');
                            if (yN) 
                                $scope.auth.loginNotFound = !resp.success;
                            else
                                $scope.auth.pop = false;
                        });
                    }
                });
            }
            $scope.nav.https && $scope.sendLogin();

            $scope.register = function() {
                console.log( 'register', $scope.auth );
                var qStr = 'user=' + $scope.auth.login + '&passwd=' + $scope.auth.passwd + '&email=' + $scope.auth.email;
                api.get('auth/register', qStr, function(regResp) {
                    console.log( ' reg resp', regResp );
                });
            }

            $scope.logout = function() {
                if (false)
                    api.get('auth/logout', '', function(resp) {
                        $scope.auth.loggedIn = !resp.success;
                        $scope.auth.pop      = !resp.success;
                        $scope.edt = false;
                    });

                loginSvc.logout($scope, function(resp){
                    $scope.auth.loggedIn = false;
                    $scope.auth.pop      = false;
                    $scope.edt = false;
                });
            };
        }
    }
}])
.controller('login', ['$scope', 'loginSvc', function($scope, loginSvc) {
    $scope.login = function() {
        loginSvc.login($scope, function(resp) {
            $scope.auth.loggedIn = resp.success;
        });
    }

    $scope.logout = function() {
        $scope.auth.loggedIn = false;
        loginSvc.logout($scope, function(resp) {});
    }
}])
.controller('appCtrl', ['$scope', 'loginSvc', function($scope, loginSvc) {
}])
;
