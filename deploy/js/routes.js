APP.config(['$routeProvider', 'TPLProvider', 'deviceDetectorProvider', 'FTPLProvider', '$locationProvider', function($routeProvider, TPLProvider, deviceDetectorProvider, FTPLProvider,$locationProvider) {

    $locationProvider.html5Mode(true);
    var routes = {};
    __ROUTESSTR.map(function(ctrl) {
        var tpl = TPLProvider.$get()['ctrl_' + ctrl];
        routes[ctrl] = { template: tpl, controller: ctrl }; 
    });

    $routeProvider
        .when('/',          routes.land) __ROUTESPROVIDER
    ;
}]);
APP
