if (typeof APP == 'undefined') var APP = angular.module('FRAME',['ngFileUpload', "ng.deviceDetector", "ngTouch", "ngRoute", "angular-sortable-view", "youtube-embed", "angularTrix"]);

typeof document.registerElement == 'undefined' || document.registerElement('pop-it');

function waitForDom(domPar, domEl, fn, cnt) {
    typeof cnt == 'undefined' && (cnt=40);
    if (cnt > 0) 
        if (domPar[domEl]) fn(); 
        else setTimeout(function() {waitForDom(domPar, domEl, fn, --cnt);}, 100);
}


function runSocket() {
    sockjs = new SockJS('/data');

    sockjs.onopen    = function()  {
        console.log('open' +  sockjs.protocol);
        restarts = 1;
        setTimeout( function() { sockjs.send(''); }, 30);
    };

    sockjs.onmessage = function(e) {
        console.log('from SOCKET', JSON.parse(e.data));
    };

    sockjs.onclose   = function()  {
        console.log('close...restarting ', restarts);
        restarts<=10 && setTimeout(function() { runSocket(); }, 3000*(restarts++));
    };
}
