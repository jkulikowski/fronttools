APP
.directive('lax', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        link: function($scope, el, attrs) {
            window.addEventListener('scroll', function(e) {
               var scroll = window.pageYOffset;
               el.css("transform","translateY(" + (1*scroll)/2 + "px)");
            });
        }
    }
}])
.directive('beforeMenu', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.menu_before,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('afterMenu', ['TPL', function(TPL) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.menu_after,
        link: function($scope, el, attrs) {
        }
    }
}])
.directive('footer', ['TPL', 'notifSvc', function(TPL, notifSvc) {
    return {
        restrict: 'A',
        replace: false,
        template: TPL.footer,
        link: function($scope, el, attrs) {

            $scope.delLink = function(idxA) {
                console.log( 'footer delLink', idxA, 'abc');
                notifSvc.warn('Remove Link, Please confirm', 10, function(yes) {
                    if (yes) 
                        $scope.cms.links[idxA[0]].splice(idxA[1],1);
                    else
                        notifSvc.info('Remove Link aborted ', 3);
                });
            }
        }
    }
}])
.filter('adjustWord', function() {
    return function(input) {
        var ret;
        switch(input) {
            case 'mo':         ret = "Monday";       break;
            case 'tu':         ret = "Tuesday";      break;
            case 'we':         ret = "Wednesday";    break;
            case 'th':         ret = "Thursday";     break;
            case 'fr':         ret = "Friday";       break;
            case 'sa':         ret = "Saturday";     break;
            case 'su':         ret = "Sunday";       break;
            default :          ret = input;          break;
        }
        return ret;
    };
})
;
